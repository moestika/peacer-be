package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.EmployeeAllDto;
import com.peacer.employeeassistantcenter.dto.EmployeeCreateDto;
import com.peacer.employeeassistantcenter.dto.EmployeeDetailDto;
import com.peacer.employeeassistantcenter.dto.EmployeeUpdateDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.EmployeeModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.EmployeeRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeImpl implements EmployeeUseCase {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<EmployeeAllDto> findAllEmployees() {
        List<EmployeeModel> employeeModelList = employeeRepository.findAll();
        List<EmployeeAllDto> employeeAllDtoList = new ArrayList<>();
        for (EmployeeModel employeeModel : employeeModelList) {
            EmployeeAllDto employeeAllDto = ModelMapperConfiguration.modelMapperConfig().map(employeeModel,EmployeeAllDto.class);
            employeeAllDtoList.add(employeeAllDto);
        }
        return employeeAllDtoList;
    }

    @Override
    public EmployeeDetailDto findEmployeeByUserId(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<EmployeeModel> employeeModelOptional = employeeRepository.findByUser(userModelOptional.get());
            if (employeeModelOptional.isPresent()) {
                EmployeeDetailDto employeeDetailDto = ModelMapperConfiguration.modelMapperConfig().map(employeeModelOptional.get(),EmployeeDetailDto.class);
                employeeDetailDto.setUserId(userId);
                employeeDetailDto.setUserEmail(userModelOptional.get().getUserEmail());
                return employeeDetailDto;
            } else {
                throw new ResourceNotFound("Employee Data not found: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public void createEmployeeData(EmployeeCreateDto employeeCreateDto) {
        EmployeeModel employeeModel = ModelMapperConfiguration.modelMapperConfig().map(employeeCreateDto,EmployeeModel.class);
        Optional<UserModel> userModelOptional = userRepository.findById(employeeCreateDto.getUserId());
        if (userModelOptional.isPresent()) {
            Optional<EmployeeModel> employeeModelOptional = employeeRepository.findByUser(userModelOptional.get());
            if (!employeeModelOptional.isPresent()) {
                employeeModel.setUser(userModelOptional.get());
                employeeRepository.save(employeeModel);
            } else {
                throw new ResourceNotFound("Employee Data already exist");
            }
        } else {
            throw new ResourceNotFound("User not found: " + employeeCreateDto.getUserId());
        }
    }

    @Override
    public void updateEmployeeData(Integer userId,EmployeeUpdateDto employeeUpdateDto) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<EmployeeModel> employeeModelOptional = employeeRepository.findByUser(userModelOptional.get());
            if (employeeModelOptional.isPresent()) {
                EmployeeModel employeeModel = ModelMapperConfiguration.modelMapperConfig().map(employeeUpdateDto,EmployeeModel.class);
                employeeRepository.save(employeeModel);
            } else {
                throw new ResourceNotFound("Employee Data not found: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public void deleteEmployeeData(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<EmployeeModel> employeeModelOptional = employeeRepository.findByUser(userModelOptional.get());
            if (employeeModelOptional.isPresent()) {
                employeeRepository.delete(employeeModelOptional.get());
            } else {
                throw new ResourceNotFound("Employee Data not found: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

}
