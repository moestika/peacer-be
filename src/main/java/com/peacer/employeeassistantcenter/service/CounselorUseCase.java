package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.CounselorAllDto;
import com.peacer.employeeassistantcenter.dto.CounselorCreateDto;
import com.peacer.employeeassistantcenter.dto.CounselorDetailDto;
import com.peacer.employeeassistantcenter.dto.CounselorUpdateDto;

import java.util.List;

public interface CounselorUseCase  {

    List<CounselorAllDto> findAllCounselors();
    CounselorDetailDto findCounselorByUserId(Integer userId);
    void createCounselorData(CounselorCreateDto counselorCreateDto);
    void updateCounselorData(Integer userId, CounselorUpdateDto counselorUpdateDto);
    void deleteCounselorData(Integer userId);

}
