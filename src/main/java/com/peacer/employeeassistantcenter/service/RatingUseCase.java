package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.RatingDto;

import java.util.List;

public interface RatingUseCase {

    List<RatingDto> findAllRatings();
    RatingDto findRatingById(Integer ratingId);
    void createRatingData(RatingDto ratingDto);
    void updateRatingData(Integer ratingId, RatingDto ratingDto);
    void deleteRatingData(Integer ratingId);

}
