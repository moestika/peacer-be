package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.CategoryDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.CategoryModel;
import com.peacer.employeeassistantcenter.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryImpl implements CategoryUseCase {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<CategoryDto> findAllCategories()
    {
        List<CategoryModel> models = categoryRepository.findAll();
        List<CategoryDto> dtoList = new ArrayList<>();
        for(CategoryModel var: models)
        {
            CategoryDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, CategoryDto.class);
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public CategoryDto findCategoryById(Integer categoryId)
    {
        Optional<CategoryModel> opt = Optional.ofNullable(categoryRepository.findById(categoryId).orElseThrow(()->new ResourceNotFound("data not found")));
        CategoryDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, CategoryDto.class);
        return dto;
    }

    @Override
    public void saveCategory(CategoryDto createDto)
    {
        CategoryModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, CategoryModel.class);
        payload.setCategoryId(createDto.getCategoryId());
        categoryRepository.save(payload);
    }

    @Override
    public void deleteCategoryById(Integer categoryId) {
        categoryRepository.deleteById(categoryId);
    }

    @Override
    public void updateCategoryData(Integer id, CategoryDto updateDto) {
        Optional<CategoryModel> opt = categoryRepository.findById(id);
        if (opt.isPresent())
        {
            CategoryModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto,CategoryModel.class);
            model.setCategoryId(id);
            categoryRepository.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }

}
