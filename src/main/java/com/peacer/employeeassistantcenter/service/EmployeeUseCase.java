package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.EmployeeAllDto;
import com.peacer.employeeassistantcenter.dto.EmployeeCreateDto;
import com.peacer.employeeassistantcenter.dto.EmployeeDetailDto;
import com.peacer.employeeassistantcenter.dto.EmployeeUpdateDto;

import java.util.List;

public interface EmployeeUseCase
{
    List<EmployeeAllDto> findAllEmployees();
    EmployeeDetailDto findEmployeeByUserId(Integer userId);
    void createEmployeeData(EmployeeCreateDto employeeCreateDto);
    void updateEmployeeData(Integer userId,EmployeeUpdateDto employeeUpdateDto);
    void deleteEmployeeData(Integer userId);
}
