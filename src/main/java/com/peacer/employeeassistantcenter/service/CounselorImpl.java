package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.*;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.CounselorModel;
import com.peacer.employeeassistantcenter.model.RatingModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.CounselorRepository;
import com.peacer.employeeassistantcenter.repository.RatingRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CounselorImpl implements CounselorUseCase {

    @Autowired
    CounselorRepository counselorRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Override
    public List<CounselorAllDto> findAllCounselors() {
        List<CounselorModel> counselorModelList = counselorRepository.findAll();
        List<CounselorAllDto> counselorAllDtoList = new ArrayList<>();
        for (CounselorModel counselorModel : counselorModelList) {
            CounselorAllDto counselorAllDto = ModelMapperConfiguration.modelMapperConfig().map(counselorModel,CounselorAllDto.class);
            List<Integer> ratingScores = new ArrayList<>();
            List<RatingModel> ratingModelList = ratingRepository.findAllByCounselor(counselorModel);
            for (RatingModel ratingModel : ratingModelList) {
                ratingScores.add(ratingModel.getRatingScore());
            }
            if (ratingScores.size()>0) {
                Double ratingTotal = 0.0;
                for (Integer ratingScore : ratingScores) {
                    ratingTotal += ratingScore;
                }
                counselorAllDto.setRatingScore(ratingTotal/ratingScores.size());
            } else {
                counselorAllDto.setRatingScore(0.0);
            }
            counselorAllDtoList.add(counselorAllDto);
        }
        return counselorAllDtoList;
    }

    @Override
    public CounselorDetailDto findCounselorByUserId(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<CounselorModel> counselorModelOptional = counselorRepository.findByUser(userModelOptional.get());
            if (counselorModelOptional.isPresent()) {
                CounselorDetailDto counselorDetailDto = ModelMapperConfiguration.modelMapperConfig().map(counselorModelOptional.get(),CounselorDetailDto.class);
                counselorDetailDto.setUserId(userId);
                List<CounselorRatingDto> counselorRatingDtoList = new ArrayList<>();
                List<Integer> ratingScores = new ArrayList<>();
                List<RatingModel> ratingModelList = ratingRepository.findAllByCounselor(counselorModelOptional.get());
                for (RatingModel ratingModel : ratingModelList) {
                    CounselorRatingDto counselorRatingDto = ModelMapperConfiguration.modelMapperConfig().map(ratingModel,CounselorRatingDto.class);
                    counselorRatingDto.setEmployeeFullName(ratingModel.getEmployee().getEmployeeFullName());
                    counselorRatingDtoList.add(counselorRatingDto);
                    ratingScores.add(ratingModel.getRatingScore());
                }
                if (ratingScores.size()>0) {
                    Double ratingTotal = 0.0;
                    for (Integer ratingScore : ratingScores) {
                        ratingTotal += ratingScore;
                    }
                    counselorDetailDto.setRatingScore(ratingTotal/ratingScores.size());
                } else {
                    counselorDetailDto.setRatingScore(0.0);
                }
                counselorDetailDto.setRatings(counselorRatingDtoList);
                return counselorDetailDto;
            } else {
                throw new ResourceNotFound("Counselor data not found: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public void createCounselorData(CounselorCreateDto counselorCreateDto) {
        Optional<UserModel> userModelOptional = userRepository.findById(counselorCreateDto.getUserId());
        if (userModelOptional.isPresent()) {
            Optional<CounselorModel> counselorModelOptional = counselorRepository.findByUser(userModelOptional.get());
            if (!counselorModelOptional.isPresent()) {
                CounselorModel counselorModel = ModelMapperConfiguration.modelMapperConfig().map(counselorCreateDto,CounselorModel.class);
                counselorModel.setUser(userModelOptional.get());
                counselorRepository.save(counselorModel);
            } else {
                throw new ResourceNotFound("Counselor data already exist: " + counselorCreateDto.getUserId());
            }
        } else {
            throw new ResourceNotFound("User not found: " + counselorCreateDto.getUserId());
        }
    }

    @Override
    public void updateCounselorData(Integer userId, CounselorUpdateDto counselorUpdateDto) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<CounselorModel> counselorModelOptional = counselorRepository.findByUser(userModelOptional.get());
            if (counselorModelOptional.isPresent()) {
                CounselorModel counselorModel = ModelMapperConfiguration.modelMapperConfig().map(counselorModelOptional,CounselorModel.class);
                counselorRepository.save(counselorModel);
            } else {
                throw new ResourceNotFound("Counselor Data not found: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public void deleteCounselorData(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<CounselorModel> counselorModelOptional = counselorRepository.findByUser(userModelOptional.get());
            if (counselorModelOptional.isPresent()) {
                counselorRepository.delete(counselorModelOptional.get());
            } else {
                throw new ResourceNotFound("Counselor Data not found: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

}
