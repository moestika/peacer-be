package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.UserAllDto;
import com.peacer.employeeassistantcenter.dto.UserCreateDto;
import com.peacer.employeeassistantcenter.dto.UserDetailDto;
import com.peacer.employeeassistantcenter.dto.UserUpdateDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.RoleModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.RoleRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserImpl implements UserUseCase {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public List<UserAllDto> findAllUsers() {
        List<UserModel> userModelList = userRepository.findAll();
        List<UserAllDto> userAllDtoList = new ArrayList<>();
        for (UserModel user : userModelList) {
            Optional<RoleModel> roleModelOptional = roleRepository.findById(user.getRole().getRoleId());
            if (roleModelOptional.isPresent()) {
                UserAllDto userAllDto = ModelMapperConfiguration.modelMapperConfig().map(user,UserAllDto.class);
                userAllDto.setUserId(user.getUserId());
                userAllDto.setUserEmail(user.getUserEmail());
                userAllDto.setUserStatus(user.getUserStatus());
                userAllDto.setUserRole(roleModelOptional.get().getRoleName());
                userAllDtoList.add(userAllDto);
            } else {
                throw new ResourceNotFound("Role not found: " + user.getRole().getRoleId());
            }
        }
        return userAllDtoList;
    }

    @Override
    public UserDetailDto findUserById(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<RoleModel> roleModelOptional = roleRepository.findById(userModelOptional.get().getRole().getRoleId());
            if (roleModelOptional.isPresent()) {
                UserDetailDto userDetailDto = ModelMapperConfiguration.modelMapperConfig().map(userModelOptional.get(),UserDetailDto.class);
                userDetailDto.setUserId(userModelOptional.get().getUserId());
                userDetailDto.setUserEmail(userModelOptional.get().getUserEmail());
                userDetailDto.setUserRole(roleModelOptional.get().getRoleName());
                userDetailDto.setUserStatus(userModelOptional.get().getUserStatus());
                return userDetailDto;
            } else {
                throw new ResourceNotFound("Role not found: " + userModelOptional.get().getRole().getRoleId());
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public UserDetailDto createUserData(UserCreateDto userCreateDto) {
        UserModel userModel = ModelMapperConfiguration.modelMapperConfig().map(userCreateDto,UserModel.class);
        Optional<UserModel> userModelOptional = userRepository.findByUserEmail(userCreateDto.getUserEmail());
        if (!userModelOptional.isPresent()) {
            Optional<RoleModel> roleModelOptional = roleRepository.findById(userCreateDto.getUserRole());
            userModel.setRole(roleModelOptional.get());
            userModel.setUserPassword(passwordEncoder.encode(userCreateDto.getUserPassword()));
            userModel.setUserStatus("active");
            userRepository.save(userModel);
            UserDetailDto userDetailDto = ModelMapperConfiguration.modelMapperConfig().map(userModel,UserDetailDto.class);
            return userDetailDto;
        } else {
            throw new ResourceNotFound("Email address already exist");
        }
    }

    @Override
    public UserDetailDto updateUserData(UserUpdateDto userUpdateDto, Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<RoleModel> roleModelOptional = roleRepository.findById(userUpdateDto.getUserRole());
            if (roleModelOptional.isPresent()) {
                UserModel userModel = userModelOptional.get();
                userModel.setUserId(userId);
                userModel.setUserEmail(userUpdateDto.getUserEmail());
                userModel.setUserPassword(passwordEncoder.encode(userUpdateDto.getUserPassword()));
                userModel.setUserStatus(userUpdateDto.getUserStatus());
                userModel.setRole(roleModelOptional.get());
                userRepository.save(userModel);
                UserDetailDto userDetailDto = ModelMapperConfiguration.modelMapperConfig().map(userModel,UserDetailDto.class);
                return userDetailDto;
            } else {
                throw new ResourceNotFound("Role not found: " + userModelOptional.get().getRole().getRoleId());
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public void deleteUserData(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            userRepository.delete(userModelOptional.get());
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }
}
