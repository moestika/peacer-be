package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PersonalityTestDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PersonalityTestModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.PersonalityTestRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonalityTestImpl implements PersonalityTestUseCase {

    @Autowired
    PersonalityTestRepository personalityTestRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<PersonalityTestDto> findAllPersonalityTests() {
        List<PersonalityTestModel> personalityTestModelList = personalityTestRepository.findAll();
        List<PersonalityTestDto> personalityTestDtoList = new ArrayList<>();
        for (PersonalityTestModel personalityTestModel : personalityTestModelList) {
            PersonalityTestDto personalityTestDto = new PersonalityTestDto();
            personalityTestDto.setPersonalityTestId(personalityTestModel.getPersonalityTestId());
            personalityTestDto.setPersonalityTestDateCreated(personalityTestModel.getPersonalityTestDateCreated());
            personalityTestDto.setPersonalityTestQuestion(personalityTestModel.getPersonalityTestQuestion());
            personalityTestDto.setPersonalityTestType(personalityTestModel.getPersonalityTestType());
            personalityTestDto.setPersonalityTestAuthor(personalityTestModel.getPersonalityTestAuthor().getUserId());
            personalityTestDtoList.add(personalityTestDto);
        }
        return personalityTestDtoList;
    }

    @Override
    public PersonalityTestDto findPersonalityTestById(Integer personalityTestId) {
        Optional<PersonalityTestModel> personalityTestModelOptional = personalityTestRepository.findById(personalityTestId);
        if (personalityTestModelOptional.isPresent()) {
            PersonalityTestDto personalityTestDto = new PersonalityTestDto();
            personalityTestDto.setPersonalityTestId(personalityTestModelOptional.get().getPersonalityTestId());
            personalityTestDto.setPersonalityTestDateCreated(personalityTestModelOptional.get().getPersonalityTestDateCreated());
            personalityTestDto.setPersonalityTestQuestion(personalityTestModelOptional.get().getPersonalityTestQuestion());
            personalityTestDto.setPersonalityTestType(personalityTestModelOptional.get().getPersonalityTestType());
            personalityTestDto.setPersonalityTestAuthor(personalityTestModelOptional.get().getPersonalityTestAuthor().getUserId());
            return personalityTestDto;
        } else {
            throw new ResourceNotFound("Personality Test not found: " + personalityTestId);
        }
    }

    @Override
    public PersonalityTestDto createPersonalityTest(PersonalityTestDto personalityTestDto) {
        PersonalityTestModel personalityTestModel = ModelMapperConfiguration.modelMapperConfig().map(personalityTestDto,PersonalityTestModel.class);
        Optional<UserModel> userModelOptional = userRepository.findById(personalityTestDto.getPersonalityTestAuthor());
        if (userModelOptional.isPresent()) {
            personalityTestModel.setPersonalityTestAuthor(userModelOptional.get());
            personalityTestRepository.save(personalityTestModel);
            PersonalityTestDto testDto = new PersonalityTestDto();
            testDto.setPersonalityTestId(personalityTestModel.getPersonalityTestId());
            testDto.setPersonalityTestDateCreated(personalityTestModel.getPersonalityTestDateCreated());
            testDto.setPersonalityTestQuestion(personalityTestModel.getPersonalityTestQuestion());
            testDto.setPersonalityTestType(personalityTestModel.getPersonalityTestType());
            testDto.setPersonalityTestAuthor(testDto.getPersonalityTestAuthor());
            return testDto;
        } else {
            throw new ResourceNotFound("User not found: " + personalityTestDto.getPersonalityTestAuthor());
        }
    }

    @Override
    public PersonalityTestDto updatePersonalityTest(Integer personalityTestId, PersonalityTestDto personalityTestDto) {
        Optional<PersonalityTestModel> personalityTestModelOptional = personalityTestRepository.findById(personalityTestId);
        if (personalityTestModelOptional.isPresent()) {
            Optional<UserModel> userModelOptional = userRepository.findById(personalityTestDto.getPersonalityTestAuthor());
            if (userModelOptional.isPresent()) {
                PersonalityTestModel personalityTestModel = ModelMapperConfiguration.modelMapperConfig().map(personalityTestModelOptional.get(),PersonalityTestModel.class);
                personalityTestModel.setPersonalityTestId(personalityTestId);
                personalityTestModel.setPersonalityTestAuthor(userModelOptional.get());
                personalityTestRepository.save(personalityTestModel);
                PersonalityTestDto testDto = new PersonalityTestDto();
                testDto.setPersonalityTestId(testDto.getPersonalityTestId());
                testDto.setPersonalityTestDateCreated(testDto.getPersonalityTestDateCreated());
                testDto.setPersonalityTestQuestion(testDto.getPersonalityTestQuestion());
                testDto.setPersonalityTestType(testDto.getPersonalityTestType());
                testDto.setPersonalityTestAuthor(testDto.getPersonalityTestAuthor());
                return testDto;
            } else {
                throw new ResourceNotFound("User not found: " + personalityTestDto.getPersonalityTestAuthor());
            }
        } else {
            throw new ResourceNotFound("Personality Test not found: " + personalityTestId);
        }
    }

    @Override
    public void deletePersonalityTestById(Integer personalityTestId) {
        personalityTestRepository.deleteById(personalityTestId);
    }

}
