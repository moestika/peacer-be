package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.GenderModel;
import com.peacer.employeeassistantcenter.repository.GenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenderImpl implements GenderUseCase {

    @Autowired
    GenderRepository genderRepository;

    @Override
    public List<GenderModel> findAllGenders() {
        return genderRepository.findAll();
    }

    @Override
    public Optional<GenderModel> findGenderById(Integer genderId) {
        return genderRepository.findById(genderId);
    }

    @Override
    public void saveGender(GenderModel genderModel) {
        genderRepository.save(genderModel);
    }

    @Override
    public void deleteGenderById(Integer genderId) {
        genderRepository.deleteById(genderId);
    }
}
