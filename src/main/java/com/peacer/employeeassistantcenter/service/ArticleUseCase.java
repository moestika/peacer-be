package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.ArticleCreateDto;
import com.peacer.employeeassistantcenter.dto.ArticleDetailDto;
import com.peacer.employeeassistantcenter.dto.ArticleViewDto;
import com.peacer.employeeassistantcenter.model.ArticleModel;

import java.util.List;
import java.util.Optional;

public interface ArticleUseCase {

    List<ArticleViewDto> findAllArticles();
    ArticleDetailDto findArticleById(Integer articleId);
    void saveArticle(ArticleCreateDto createDto);
    void deleteArticleById(Integer articleId);
    void updateArticleData(Integer id, ArticleCreateDto updateDto);
}
