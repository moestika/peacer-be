package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.PersonalityUserAnswerCreateDto;
import com.peacer.employeeassistantcenter.dto.PersonalityUserAnswerViewDto;
import com.peacer.employeeassistantcenter.model.PersonalityUserAnswerModel;

import java.util.List;

public interface PersonalityUserAnswerUseCase
{
    List<PersonalityUserAnswerViewDto> findAllPersonalityUserAnswers();
    PersonalityUserAnswerViewDto findPersonalityUserAnswersById(Integer answerId, Integer userId);
    void savePersonalityUserAnswer(PersonalityUserAnswerCreateDto dto);
    void deletePersonalityUserAnswers(Integer id);
    void updatePersonalityUserAnswer(Integer answerId, Integer userId, PersonalityUserAnswerCreateDto updateDto);
}
