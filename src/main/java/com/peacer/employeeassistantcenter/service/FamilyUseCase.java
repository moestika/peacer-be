package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.FamilyDto;

import java.util.List;

public interface FamilyUseCase {

    List<FamilyDto> findAllFamilies();
    List<FamilyDto> findAllFamiliesByUserId(Integer userId);
    FamilyDto findFamilyById(Integer familyId);
    void createFamilyData(FamilyDto familyDto);
    FamilyDto updateFamilyData(Integer familyId, FamilyDto familyDto);
    void deleteFamilyDataById(Integer familyId);

}
