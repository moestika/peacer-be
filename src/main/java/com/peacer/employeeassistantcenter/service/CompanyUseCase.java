package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.CompanyModel;

import java.util.List;
import java.util.Optional;

public interface CompanyUseCase {

    List<CompanyModel> findAllCompanies();
    Optional<CompanyModel> findCompanyById(Integer companyId);
    void saveCompany(CompanyModel companyModel);
    void deleteCompanyById(Integer companyId);

}
