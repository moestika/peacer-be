package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.UserAllDto;
import com.peacer.employeeassistantcenter.dto.UserCreateDto;
import com.peacer.employeeassistantcenter.dto.UserDetailDto;
import com.peacer.employeeassistantcenter.dto.UserUpdateDto;

import java.util.List;

public interface UserUseCase  {

    List<UserAllDto> findAllUsers();
    UserDetailDto findUserById(Integer id);
    UserDetailDto createUserData(UserCreateDto userCreateDto);
    UserDetailDto updateUserData(UserUpdateDto userUpdateDto, Integer userId);
    void deleteUserData(Integer userId);

}
