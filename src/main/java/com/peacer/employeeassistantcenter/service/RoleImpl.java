package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.RoleDto;
import com.peacer.employeeassistantcenter.dto.RoleDetailDto;
import com.peacer.employeeassistantcenter.dto.UserRoleDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.RoleModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleImpl implements RoleUseCase {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public List<RoleDto> findAllRoles() {
        List<RoleModel> roleModelList = roleRepository.findAll();
        List<RoleDto> roleDtoList = new ArrayList<>();
        for (RoleModel roleModel : roleModelList) {
            RoleDto roleDto = ModelMapperConfiguration.modelMapperConfig().map(roleModel, RoleDto.class);
            roleDtoList.add(roleDto);
        }
        return roleDtoList;
    }

    @Override
    public RoleDetailDto findRoleById(Integer roleId) {
        Optional<RoleModel> roleModelOptional = roleRepository.findById(roleId);
        if (roleModelOptional.isPresent()) {
            RoleDetailDto roleDetailDto = ModelMapperConfiguration.modelMapperConfig().map(roleModelOptional.get(),RoleDetailDto.class);
            List<UserRoleDto> userRoleDtoList = new ArrayList<>();
            for (UserModel user: roleModelOptional.get().getUsers()) {
                UserRoleDto userRoleDto = ModelMapperConfiguration.modelMapperConfig().map(user,UserRoleDto.class);
                userRoleDto.setUserId(user.getUserId());
                userRoleDto.setUserEmail(user.getUserEmail());
                userRoleDtoList.add(userRoleDto);
            }
            return roleDetailDto;
        } else {
            throw new ResourceNotFound("Role not found: " + roleId);
        }
    }

    @Override
    public void createRoleData(RoleDto roleDto) {
        RoleModel roleModel = ModelMapperConfiguration.modelMapperConfig().map(roleDto,RoleModel.class);
        Optional<RoleModel> roleModelOptional = roleRepository.findByRoleName(roleDto.getRoleName());
        if (!roleModelOptional.isPresent()) {
            roleRepository.save(roleModel);
        } else {
            throw new ResourceNotFound("Role already exist: " + roleDto.getRoleName());
        }
    }

    @Override
    public void updateRoleData(Integer roleId, RoleDto roleDto) {
        Optional<RoleModel> roleModelOptional = roleRepository.findById(roleDto.getRoleId());
        if (roleModelOptional.isPresent()) {
            RoleModel roleModel = roleModelOptional.get();
            roleModel.setRoleName(roleDto.getRoleName());
            roleRepository.save(roleModel);
        } else {
            throw new ResourceNotFound("Role not found: " + roleId);
        }
    }

    @Override
    public void deleteRoleData(Integer roleId) {
        Optional<RoleModel> roleModelOptional = roleRepository.findById(roleId);
        if (roleModelOptional.isPresent()) {
            roleRepository.deleteById(roleId);
        } else {
            throw new ResourceNotFound("Role not found: " + roleId);
        }
    }

}
