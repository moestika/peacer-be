package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PopupFeelingCreateDto;
import com.peacer.employeeassistantcenter.dto.PopupFeelingViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PopupFeelingModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.PopupFeelingsRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PopupFeelingImpl implements PopupFeelingUseCase
{
    @Autowired
    PopupFeelingsRepository repo;
    @Autowired
    UserRepository userRepository;

    @Override
    public List<PopupFeelingViewDto> findAllPopup()
    {
        List<PopupFeelingModel> list = repo.findAll();
        List<PopupFeelingViewDto> dtoList = new ArrayList<>();
        for (PopupFeelingModel var: list)
        {
            Optional<UserModel> opt = Optional.ofNullable(userRepository.findById(var.getUser().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            if(opt.isPresent())
            {
                PopupFeelingViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, PopupFeelingViewDto.class);
                dto.setUserName(opt.get().getUserEmail());
                dto.setUserId(opt.get().getUserId());
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    @Override
    public PopupFeelingViewDto findPopupById(Integer id)
    {
        Optional<PopupFeelingModel> opt = Optional.ofNullable(repo.findById(id).orElseThrow(()->new ResourceNotFound("data not found")));
        PopupFeelingViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, PopupFeelingViewDto.class);
        dto.setUserName(opt.get().getUser().getUserEmail());
        dto.setUserId(opt.get().getUser().getUserId());
        return dto;
    }

    @Override
    public void savePopupData(PopupFeelingCreateDto createDto)
    {
        PopupFeelingModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, PopupFeelingModel.class);
        Optional<UserModel> opt = Optional.ofNullable(userRepository.findById(createDto.getPopupUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setPopupId(createDto.getPopupId());
        payload.setUser(opt.get());
        repo.save(payload);
    }

    @Override
    public void deletePopupData(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public void updatePopupData(Integer id, PopupFeelingCreateDto updateDto)
    {
        Optional<PopupFeelingModel> opt = repo.findById(id);
        if (opt.isPresent())
        {
            PopupFeelingModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, PopupFeelingModel.class);
            model.setPopupId(id);
            repo.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
