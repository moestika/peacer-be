package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.ArticleCreateDto;
import com.peacer.employeeassistantcenter.dto.ArticleDetailDto;
import com.peacer.employeeassistantcenter.dto.ArticleViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.ArticleModel;
import com.peacer.employeeassistantcenter.model.CategoryModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.ArticleRepository;
import com.peacer.employeeassistantcenter.repository.CategoryRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ArticleImpl implements ArticleUseCase {

    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<ArticleViewDto> findAllArticles()
    {
        List<ArticleModel> models = articleRepository.findAll();
        List<ArticleViewDto> dtoList = new ArrayList<>();
        for(ArticleModel var: models)
        {
            Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(var.getUser().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<CategoryModel> optCategory = Optional.ofNullable(categoryRepository.findById(var.getCategory().getCategoryId()).orElseThrow(()->new ResourceNotFound("data not found")));
            ArticleViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, ArticleViewDto.class);
            dto.setCreatorUserName(optUser.get().getUserEmail());
            dto.setCategoryName(optCategory.get().getCategoryName());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public ArticleDetailDto findArticleById(Integer articleId)
    {
        Optional<ArticleModel> opt = Optional.ofNullable(articleRepository.findById(articleId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(opt.get().getUser().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<CategoryModel> optCategory = Optional.ofNullable(categoryRepository.findById(opt.get().getCategory().getCategoryId()).orElseThrow(()->new ResourceNotFound("data not found")));
        ArticleDetailDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, ArticleDetailDto.class);
        dto.setCreatorUserId(optUser.get().getUserId());
        dto.setCreatorUserName(optUser.get().getUserEmail());
        dto.setCategoryId(optCategory.get().getCategoryId());
        dto.setCategoryName(optCategory.get().getCategoryName());
        return dto;
    }

    @Override
    public void saveArticle(ArticleCreateDto createDto)
    {
        ArticleModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, ArticleModel.class);
        Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(createDto.getCreatorUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<CategoryModel> optCategory = Optional.ofNullable(categoryRepository.findById(createDto.getCategoryId()).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setArticleId(createDto.getArticleId());
        payload.setUser(optUser.get());
        payload.setCategory(optCategory.get());
        articleRepository.save(payload);
    }

    @Override
    public void deleteArticleById(Integer articleId) {
        articleRepository.deleteById(articleId);
    }

    @Override
    public void updateArticleData(Integer id, ArticleCreateDto updateDto)
    {
        Optional<ArticleModel> opt = Optional.ofNullable(articleRepository.findById(id).orElseThrow(()->new ResourceNotFound("data not found")));
        ArticleModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, ArticleModel.class);
        model.setArticleId(id);
        articleRepository.save(model);
    }

}
