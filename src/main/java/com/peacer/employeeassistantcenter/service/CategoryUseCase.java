package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.CategoryDto;
import com.peacer.employeeassistantcenter.model.CategoryModel;

import java.util.List;
import java.util.Optional;

public interface CategoryUseCase {

    List<CategoryDto> findAllCategories();
    CategoryDto findCategoryById(Integer categoryId);
    void saveCategory(CategoryDto createDto);
    void deleteCategoryById(Integer categoryId);
    void updateCategoryData(Integer id, CategoryDto updateDto);
}
