package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.QuestionTreeCreateDto;
import com.peacer.employeeassistantcenter.dto.QuestionTreeViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.OptionModel;
import com.peacer.employeeassistantcenter.model.QuestionTreeModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.OptionRepository;
import com.peacer.employeeassistantcenter.repository.QuestionTreeRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionTreeImpl implements QuestionTreeUseCase
{
    @Autowired
    QuestionTreeRepository repo;
    @Autowired
    OptionRepository optionRepository;
    @Autowired
    UserRepository userRepository;

    @Override
    public List<QuestionTreeViewDto> findAllQuestionTree()
    {
        List<QuestionTreeModel> list = repo.findAll();
        List<QuestionTreeViewDto> dtoList = new ArrayList<>();
        for (QuestionTreeModel var: list)
        {
            if (var.getOption() == null)
            {
                Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(var.getCreator().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
                if (optUser.isPresent())
                {
                    QuestionTreeViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, QuestionTreeViewDto.class);
                    dto.setQuestionTreeId(var.getQuestionTreeId());
                    dto.setQuestionDate(var.getQuestionDate());
                    dto.setQuestion(var.getQuestion());
                    dto.setCreatorUserName(optUser.get().getUserEmail());
                    dtoList.add(dto);
                }
            }
            else
            {
                Optional<OptionModel> optionModel = Optional.ofNullable(optionRepository.findById(var.getOption().getOptionId()).orElseThrow(()->new ResourceNotFound("data not found")));
                if(optionModel.isPresent())
                {
                    Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(var.getCreator().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
                    if (optUser.isPresent())
                    {
                        QuestionTreeViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, QuestionTreeViewDto.class);
                        dto.setQuestionTreeId(var.getQuestionTreeId());
                        dto.setQuestionDate(var.getQuestionDate());
                        dto.setQuestion(var.getQuestion());
                        dto.setOptionParent(optionModel.get().getSolution());
                        dto.setCreatorUserName(optUser.get().getUserEmail());
                        dtoList.add(dto);
                    }
                }
            }
        }
        return dtoList;
    }

    @Override
    public QuestionTreeViewDto findQuestionTreeById(Integer id)
    {
        Optional<QuestionTreeModel> opt = Optional.ofNullable(repo.findById(id).orElseThrow(()->new ResourceNotFound("data not found")));
        QuestionTreeViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, QuestionTreeViewDto.class);
        if(opt.get().getOption() == null)
        {
            Optional<UserModel> userModel = Optional.ofNullable(userRepository.findById(opt.get().getCreator().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            dto.setCreatorUserName(userModel.get().getUserEmail());
            dto.setCreatorUserId(userModel.get().getUserId());
        }
        else
        {
            Optional<UserModel> userModel = Optional.ofNullable(userRepository.findById(opt.get().getCreator().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<OptionModel> optionModel = Optional.ofNullable(optionRepository.findById(opt.get().getOption().getOptionId()).orElseThrow(()->new ResourceNotFound("data not found")));
            dto.setCreatorUserName(userModel.get().getUserEmail());
            dto.setCreatorUserId(userModel.get().getUserId());
            dto.setOptionParent(optionModel.get().getSolution());
            dto.setOptionId(optionModel.get().getOptionId());
        }
        return dto;
    }

    @Override
    public void saveQuestionTreeData(QuestionTreeCreateDto createDto)
    {
        QuestionTreeModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, QuestionTreeModel.class);
        if(createDto.getOptionParentId() == null)
        {
            Optional<UserModel> userModel = Optional.ofNullable(userRepository.findById(createDto.getCreatorUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            payload.setQuestionTreeId(createDto.getQuestionId());
            payload.setCreator(userModel.get());
        }
        else
        {
            Optional<OptionModel> optionModel = Optional.ofNullable(optionRepository.findById(createDto.getOptionParentId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<UserModel> userModel = Optional.ofNullable(userRepository.findById(createDto.getCreatorUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            payload.setQuestionTreeId(createDto.getQuestionId());
            payload.setCreator(userModel.get());
            payload.setOption(optionModel.get());
        }
        repo.save(payload);
    }

    @Override
    public void deleteQuestionTreeData(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public void updateQuestionTreeData(Integer id, QuestionTreeCreateDto updateDto)
    {
        Optional<QuestionTreeModel> opt = repo.findById(id);
        if (opt.isPresent())
        {
            QuestionTreeModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, QuestionTreeModel.class);
            model.setQuestionTreeId(id);
            repo.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
