package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.CompanyModel;
import com.peacer.employeeassistantcenter.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyImpl implements CompanyUseCase {

    @Autowired
    CompanyRepository companyRepository;

    @Override
    public List<CompanyModel> findAllCompanies() {
        return companyRepository.findAll();
    }

    @Override
    public Optional<CompanyModel> findCompanyById(Integer companyId) {
        return companyRepository.findById(companyId);
    }

    @Override
    public void saveCompany(CompanyModel companyModel) {
        companyRepository.save(companyModel);
    }

    @Override
    public void deleteCompanyById(Integer companyId) {
        companyRepository.deleteById(companyId);
    }

}
