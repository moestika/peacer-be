package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.RatingDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.CounselorModel;
import com.peacer.employeeassistantcenter.model.EmployeeModel;
import com.peacer.employeeassistantcenter.model.RatingModel;
import com.peacer.employeeassistantcenter.repository.CounselorRepository;
import com.peacer.employeeassistantcenter.repository.EmployeeRepository;
import com.peacer.employeeassistantcenter.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RatingImpl implements RatingUseCase {

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CounselorRepository counselorRepository;

    @Override
    public List<RatingDto> findAllRatings() {
        List<RatingModel> ratingModelList = ratingRepository.findAll();
        List<RatingDto> ratingDtoList = new ArrayList<>();
        for (RatingModel ratingModel : ratingModelList) {
            RatingDto ratingDto = ModelMapperConfiguration.modelMapperConfig().map(ratingModel,RatingDto.class);
            ratingDtoList.add(ratingDto);
        }
        return ratingDtoList;
    }

    @Override
    public RatingDto findRatingById(Integer ratingId) {
        Optional<RatingModel> ratingModelOptional = ratingRepository.findById(ratingId);
        if (ratingModelOptional.isPresent()) {
            RatingDto ratingDto = ModelMapperConfiguration.modelMapperConfig().map(ratingModelOptional.get(),RatingDto.class);
            return ratingDto;
        } else {
            throw new ResourceNotFound("Rating Data not found");
        }
    }

    @Override
    public void createRatingData(RatingDto ratingDto) {
        RatingModel ratingModel = ModelMapperConfiguration.modelMapperConfig().map(ratingDto,RatingModel.class);
        Optional<EmployeeModel> employeeModel = employeeRepository.findById(ratingDto.getEmployeeId());
        if (employeeModel.isPresent()) {
            if (ratingDto.getEmployeeCounselorId() == null) {
                Optional<CounselorModel> counselorModelOptional = counselorRepository.findById(ratingDto.getCounselorId());
                if (counselorModelOptional.isPresent()) {
                    ratingModel.setCounselor(counselorModelOptional.get());
                } else {
                    throw new ResourceNotFound("Counselor Data not found: " + ratingDto.getCounselorId());
                }
            } else {
                Optional<EmployeeModel> employeeModelOptional = employeeRepository.findById(ratingDto.getEmployeeCounselorId());
                if (employeeModelOptional.isPresent()) {
                    ratingModel.setEmployeeCounselor(employeeModelOptional.get());
                } else {
                    throw new ResourceNotFound("Counselor data not found: " + ratingDto.getEmployeeCounselorId());
                }
            }
            ratingModel.setEmployee(employeeModel.get());
            ratingRepository.save(ratingModel);
        } else {
            throw new ResourceNotFound("Employee Data not found: " + ratingDto.getEmployeeId());
        }
        ratingRepository.save(ratingModel);
    }

    @Override
    public void updateRatingData(Integer ratingId, RatingDto ratingDto) {
        Optional<RatingModel> ratingModelOptional = ratingRepository.findById(ratingId);
        if (ratingModelOptional.isPresent()) {
            RatingModel ratingModel = ModelMapperConfiguration.modelMapperConfig().map(ratingDto,RatingModel.class);
            ratingModel.setRatingId(ratingId);
            ratingRepository.save(ratingModel);
        } else {
            throw new ResourceNotFound("Rating Data not found: " + ratingId);
        }
    }

    @Override
    public void deleteRatingData(Integer ratingId) {
        Optional<RatingModel> ratingModel = ratingRepository.findById(ratingId);
        if (ratingModel.isPresent()) {
            ratingRepository.deleteById(ratingId);
        } else {
            throw new ResourceNotFound("Rating Data not found: " + ratingId);
        }
    }

}
