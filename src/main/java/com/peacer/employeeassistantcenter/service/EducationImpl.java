package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.EducationModel;
import com.peacer.employeeassistantcenter.repository.EducationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EducationImpl implements EducationUseCase
{
    @Autowired
    EducationRepository educationRepository;

    @Override
    public List<EducationModel> findAllEducation() {
        return educationRepository.findAll();
    }

    @Override
    public Optional<EducationModel> findEducationById(Integer id) {
        return educationRepository.findById(id);
    }

    @Override
    public void saveEducationData(EducationModel model) {
        educationRepository.save(model);
    }

    @Override
    public void deleteEducationData(EducationModel model) {
        educationRepository.delete(model);
    }
}
