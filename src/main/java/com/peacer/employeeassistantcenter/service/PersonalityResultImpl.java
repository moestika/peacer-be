package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.model.PersonalityResultModel;
import com.peacer.employeeassistantcenter.repository.PersonalityResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonalityResultImpl implements PersonalityResultUseCase {

    @Autowired
    PersonalityResultRepository personalityResultRepository;

    @Override
    public List<PersonalityResultModel> findAllPersonalityResults() {
        return personalityResultRepository.findAll();
    }

    @Override
    public Optional<PersonalityResultModel> findPersonalityResultById(Integer personalityResultId) {
        return personalityResultRepository.findById(personalityResultId);
    }

    @Override
    public void savePersonalityResult(PersonalityResultModel model)
    {
        personalityResultRepository.save(model);
    }

    @Override
    public void deletePersonalityResultById(Integer personalityResultId) {
        personalityResultRepository.deleteById(personalityResultId);
    }

}
