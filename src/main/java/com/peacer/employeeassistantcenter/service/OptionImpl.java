package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.OptionDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.OptionModel;
import com.peacer.employeeassistantcenter.model.QuestionTreeModel;
import com.peacer.employeeassistantcenter.repository.OptionRepository;
import com.peacer.employeeassistantcenter.repository.QuestionTreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OptionImpl implements OptionUseCase
{
    @Autowired
        OptionRepository repo;
        @Autowired
        QuestionTreeRepository treeRepository;

        @Override
        public List<OptionDto> findAllOptions()
        {
            List<OptionModel> model = repo.findAll();
            List<OptionDto> dtoList = new ArrayList<>();
            for (OptionModel var: model)
            {
                Optional<QuestionTreeModel> opt = Optional.ofNullable(treeRepository.findById(var.getQuestionTree().getQuestionTreeId()).orElseThrow(()->new ResourceNotFound("data not found")));
                if(opt.isPresent())
                {
                    OptionDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, OptionDto.class);
                    dto.setOptionId(var.getOptionId());
                    dto.setOptionType(var.getOptionType());
                    dto.setSolution(var.getSolution());
                    dto.setQuestionId(opt.get().getQuestionTreeId());
                    dtoList.add(dto);
                }
            }
            return dtoList;
        }

        @Override
        public OptionDto findOptionById(Integer id)
        {
            Optional<OptionModel> opt = Optional.ofNullable(repo.findById(id).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<QuestionTreeModel> optTree = Optional.ofNullable(treeRepository.findById(opt.get().getQuestionTree().getQuestionTreeId()).orElseThrow(()->new ResourceNotFound("data not found")));
            OptionDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, OptionDto.class);
            dto.setOptionId(opt.get().getOptionId());
            dto.setOptionType(opt.get().getOptionType());
            dto.setSolution(opt.get().getSolution());
            dto.setQuestionId(optTree.get().getQuestionTreeId());
            return dto;
        }

        @Override
        public void saveOptionData(OptionDto createDto)
        {
            OptionModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, OptionModel.class);
            Optional<QuestionTreeModel> optTree = Optional.ofNullable(treeRepository.findById(createDto.getQuestionId()).orElseThrow(()->new ResourceNotFound("data not found")));
            payload.setOptionId(null);
            payload.setQuestionTree(optTree.get());
            repo.save(payload);
        }

        @Override
        public void deleteOptionData(Integer id) {
            repo.deleteById(id);
        }

    @Override
    public void updateOptionData(Integer id, OptionDto updateDto)
    {
        Optional<OptionModel> opt = repo.findById(id);
        if (opt.isPresent())
        {
            OptionModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, OptionModel.class);
            model.setOptionId(id);
            repo.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
