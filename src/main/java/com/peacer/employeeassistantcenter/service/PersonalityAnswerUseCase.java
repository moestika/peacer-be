package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.PersonalityAnswerDto;
import com.peacer.employeeassistantcenter.model.PersonalityAnswerModel;

import java.util.List;
import java.util.Optional;

public interface PersonalityAnswerUseCase
{
    List<PersonalityAnswerDto> findAllPersonalityAnswer();
    PersonalityAnswerDto findPersonalityAnswerById(Integer id);
    void savePersonalityAnswerData(PersonalityAnswerDto createDto);
    void deletePersonalityAnswerData(Integer id);
    void updatePersonalityAnswerData(Integer id, PersonalityAnswerDto updateDto);
}
