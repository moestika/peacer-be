package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PersonalityUserAnswerCreateDto;
import com.peacer.employeeassistantcenter.dto.PersonalityUserAnswerViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PersonalityAnswerModel;
import com.peacer.employeeassistantcenter.model.PersonalityUserAnswerModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.PersonalityAnswerRepository;
import com.peacer.employeeassistantcenter.repository.PersonalityUserAnswerRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonalityUserAnswerImpl implements PersonalityUserAnswerUseCase
{
    @Autowired
    PersonalityUserAnswerRepository repo;
    @Autowired
    PersonalityAnswerRepository answerRepo;
    @Autowired
    UserRepository userRepo;

    @Override
    public List<PersonalityUserAnswerViewDto> findAllPersonalityUserAnswers()
    {
        List<PersonalityUserAnswerModel> modelList = repo.findAll();
        List<PersonalityUserAnswerViewDto> dtoList = new ArrayList<>();
        for (PersonalityUserAnswerModel var: modelList)
        {
            Optional<PersonalityAnswerModel> optAnswer = Optional.ofNullable(answerRepo.findById(var.getUserAnswer().getAnswerId()).orElseThrow(()->new ResourceNotFound("data not found")));
            if (optAnswer.isPresent())
            {
                Optional<UserModel> optUser = Optional.ofNullable(userRepo.findById(var.getUserPicks().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
                if (optUser.isPresent())
                {
                    PersonalityUserAnswerViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, PersonalityUserAnswerViewDto.class);
                    dto.setId(var.getId());
                    dto.setAnswerDate(var.getAnswerDate());
                    dto.setUserName(var.getUserPicks().getUserEmail());
                    dto.setAnswer(var.getUserAnswer().getAnswer());
                    dtoList.add(dto);
                }
            }
        }
        return dtoList;
    }

    @Override
    public PersonalityUserAnswerViewDto findPersonalityUserAnswersById(Integer answerId, Integer userId)
    {
        Optional<PersonalityAnswerModel> answerModel = Optional.ofNullable(answerRepo.findById(answerId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> userModel = Optional.ofNullable(userRepo.findById(userId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<PersonalityUserAnswerModel> opt = Optional.ofNullable(repo.findByUserAnswerAndUserPicks(answerModel.get(), userModel.get()).orElseThrow(()->new ResourceNotFound("data not found")));
        PersonalityUserAnswerViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, PersonalityUserAnswerViewDto.class);
        dto.setId(opt.get().getId());
        dto.setAnswerDate(opt.get().getAnswerDate());
        dto.setUserName(userModel.get().getUserEmail());
        dto.setAnswer(answerModel.get().getAnswer());
        return dto;
    }

    @Override
    public void savePersonalityUserAnswer(PersonalityUserAnswerCreateDto createDto)
    {
        PersonalityUserAnswerModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, PersonalityUserAnswerModel.class);
        Integer userId = createDto.getUserId();
        Integer answerId = createDto.getAnswerId();
        Optional<PersonalityAnswerModel> optAnswer = Optional.ofNullable(answerRepo.findById(answerId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optUser = Optional.ofNullable(userRepo.findById(userId).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setId(createDto.getId());
        payload.setUserAnswer(optAnswer.get());
        payload.setUserPicks(optUser.get());
        repo.save(payload);
    }

    @Override
    public void deletePersonalityUserAnswers(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public void updatePersonalityUserAnswer(Integer answerId, Integer userId, PersonalityUserAnswerCreateDto updateDto)
    {
        Optional<PersonalityAnswerModel> answerModel = Optional.ofNullable(answerRepo.findById(answerId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> userModel = Optional.ofNullable(userRepo.findById(userId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<PersonalityUserAnswerModel> opt = repo.findByUserAnswerAndUserPicks(answerModel.get(), userModel.get());
        if (opt.isPresent())
        {
            Optional<PersonalityAnswerModel> newAnswer = Optional.ofNullable(answerRepo.findById(updateDto.getAnswerId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<UserModel> newUser = Optional.ofNullable(userRepo.findById(updateDto.getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            PersonalityUserAnswerModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, PersonalityUserAnswerModel.class);
            model.setId(opt.get().getId());
            model.setUserPicks(newUser.get());
            model.setUserAnswer(newAnswer.get());
            model.setAnswerDate(updateDto.getAnswerDate());
            repo.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
