package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.PopupFeelingCreateDto;
import com.peacer.employeeassistantcenter.dto.PopupFeelingViewDto;

import java.util.List;

public interface PopupFeelingUseCase
{
    List<PopupFeelingViewDto> findAllPopup();
    PopupFeelingViewDto findPopupById(Integer id);
    void savePopupData(PopupFeelingCreateDto createDto);
    void deletePopupData(Integer id);
    void updatePopupData(Integer id, PopupFeelingCreateDto updateDto);
}
