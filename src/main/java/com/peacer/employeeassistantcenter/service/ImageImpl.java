package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.ImageDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.ArticleModel;
import com.peacer.employeeassistantcenter.model.ImageModel;
import com.peacer.employeeassistantcenter.repository.ArticleRepository;
import com.peacer.employeeassistantcenter.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ImageImpl implements ImageUseCase {

    @Autowired
    ImageRepository imageRepository;
    @Autowired
    ArticleRepository articleRepository;

    @Override
    public List<ImageDto> findAllImages()
    {
        List<ImageModel> models = imageRepository.findAll();
        List<ImageDto> dtoList = new ArrayList<>();
        for(ImageModel var: models)
        {
            Optional<ArticleModel> optArticle = Optional.ofNullable(articleRepository.findById(var.getArticle().getArticleId()).orElseThrow(()->new ResourceNotFound("data not found")));
            ImageDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, ImageDto.class);
            dto.setArticleId(optArticle.get().getArticleId());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public ImageDto findImageById(Integer imageId)
    {
        Optional<ImageModel> opt = Optional.ofNullable(imageRepository.findById(imageId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<ArticleModel> optArticle = Optional.ofNullable(articleRepository.findById(opt.get().getArticle().getArticleId()).orElseThrow(()->new ResourceNotFound("data not found")));
        ImageDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, ImageDto.class);
        dto.setArticleId(optArticle.get().getArticleId());
        dto.setImageId(opt.get().getImageId());
        dto.setImageUrl(opt.get().getImageUrl());
        return dto;
    }

    @Override
    public void saveImage(ImageDto createDto)
    {
        ImageModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, ImageModel.class);
        Optional<ArticleModel> optArticle = Optional.ofNullable(articleRepository.findById(createDto.getArticleId()).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setImageId(createDto.getImageId());
        payload.setArticle(optArticle.get());
        imageRepository.save(payload);
    }

    @Override
    public void deleteImageById(Integer imageId) {
        imageRepository.deleteById(imageId);
    }

    @Override
    public void updateImage(Integer id, ImageDto updateDto)
    {
        Optional<ImageModel> opt = imageRepository.findById(id);
        if (opt.isPresent())
        {
            ImageModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, ImageModel.class);
            model.setImageId(id);
            imageRepository.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }

    }

}
