package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.QuestionPoolCreateDto;
import com.peacer.employeeassistantcenter.dto.QuestionPoolDetailDto;
import com.peacer.employeeassistantcenter.dto.QuestionPoolViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.CategoryModel;
import com.peacer.employeeassistantcenter.model.QuestionPoolModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.CategoryRepository;
import com.peacer.employeeassistantcenter.repository.QuestionPoolRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionPoolImpl implements QuestionPoolUseCase {

    @Autowired
    QuestionPoolRepository questionPoolRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<QuestionPoolViewDto> findAllQuestionPools()
    {
        List<QuestionPoolModel> models = questionPoolRepository.findAll();
        List<QuestionPoolViewDto> dtoList = new ArrayList<>();
        for(QuestionPoolModel var: models)
        {
            Optional<UserModel> optCreator = Optional.ofNullable(userRepository.findById(var.getCreator().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<UserModel> optMover = Optional.ofNullable(userRepository.findById(var.getMover().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<UserModel> optResponse = Optional.ofNullable(userRepository.findById(var.getResponder().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<CategoryModel> optCategory = Optional.ofNullable(categoryRepository.findById(var.getCategory().getCategoryId()).orElseThrow(()->new ResourceNotFound("data not found")));

            QuestionPoolViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, QuestionPoolViewDto.class);
            dto.setCreatorUserName(optCreator.get().getUserEmail());
            dto.setMoverUserName(optMover.get().getUserEmail());
            dto.setResponseUserName(optResponse.get().getUserEmail());
            dto.setQuestionCategory(optCategory.get().getCategoryName());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public QuestionPoolDetailDto findQuestionPoolById(Integer questionPoolId)
    {
        Optional<QuestionPoolModel> opt = Optional.ofNullable(questionPoolRepository.findById(questionPoolId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optCreator = Optional.ofNullable(userRepository.findById(opt.get().getCreator().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optMover = Optional.ofNullable(userRepository.findById(opt.get().getMover().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optResponse = Optional.ofNullable(userRepository.findById(opt.get().getResponder().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<CategoryModel> optCategory = Optional.ofNullable(categoryRepository.findById(opt.get().getCategory().getCategoryId()).orElseThrow(()->new ResourceNotFound("data not found")));

        QuestionPoolDetailDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, QuestionPoolDetailDto.class);
        dto.setCreatorUserId(optCreator.get().getUserId());
        dto.setCreatorUserName(optCreator.get().getUserEmail());
        dto.setMoverUserId(optMover.get().getUserId());
        dto.setMoverUserName(optMover.get().getUserEmail());
        dto.setResponseUserId(optResponse.get().getUserId());
        dto.setResponseUserName(optResponse.get().getUserEmail());
        dto.setQuestionCategoryId(optCategory.get().getCategoryId());
        dto.setQuestionCategory(optCategory.get().getCategoryName());

        return dto;
    }

    @Override
    public void saveQuestionPool(QuestionPoolCreateDto createDto)
    {
        QuestionPoolModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, QuestionPoolModel.class);
        Optional<UserModel> optCreator = Optional.ofNullable(userRepository.findById(createDto.getCreatorUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optMover = Optional.ofNullable(userRepository.findById(createDto.getMoverUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optResponse = Optional.ofNullable(userRepository.findById(createDto.getResponseUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<CategoryModel> optCategory = Optional.ofNullable(categoryRepository.findById(createDto.getQuestionCategoryId()).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setQuestionPoolId(createDto.getQuestionId());
        payload.setCreator(optCreator.get());
        payload.setMover(optMover.get());
        payload.setResponder(optResponse.get());
        payload.setCategory(optCategory.get());
        questionPoolRepository.save(payload);
    }

    @Override
    public void deleteQuestionPoolById(Integer questionPoolId) {
        questionPoolRepository.deleteById(questionPoolId);
    }

    @Override
    public void updateQuestionPool(Integer id, QuestionPoolCreateDto updateDto)
    {
        Optional<QuestionPoolModel> opt = questionPoolRepository.findById(id);
        if (opt.isPresent())
        {
            QuestionPoolModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto,QuestionPoolModel.class);
            model.setQuestionPoolId(id);
            questionPoolRepository.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
