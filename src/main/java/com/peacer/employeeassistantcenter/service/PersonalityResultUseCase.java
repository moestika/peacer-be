package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.PersonalityResultModel;

import java.util.List;
import java.util.Optional;

public interface PersonalityResultUseCase {

    List<PersonalityResultModel> findAllPersonalityResults();
    Optional<PersonalityResultModel> findPersonalityResultById(Integer personalityResultId);
    void savePersonalityResult(PersonalityResultModel model);
    void deletePersonalityResultById(Integer personalityResultId);

}
