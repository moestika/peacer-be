package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.PersonalityTestDto;

import java.util.List;

public interface PersonalityTestUseCase {

    List<PersonalityTestDto> findAllPersonalityTests();
    PersonalityTestDto findPersonalityTestById(Integer personalityTestId);
    PersonalityTestDto createPersonalityTest(PersonalityTestDto createDto);
    PersonalityTestDto updatePersonalityTest(Integer personalityTestId, PersonalityTestDto createDto);
    void deletePersonalityTestById(Integer personalityTestId);
    
}
