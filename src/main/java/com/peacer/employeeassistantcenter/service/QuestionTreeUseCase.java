package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.QuestionTreeCreateDto;
import com.peacer.employeeassistantcenter.dto.QuestionTreeViewDto;
import com.peacer.employeeassistantcenter.model.QuestionTreeModel;

import java.util.List;
import java.util.Optional;

public interface QuestionTreeUseCase
{
    List<QuestionTreeViewDto> findAllQuestionTree();
    QuestionTreeViewDto findQuestionTreeById(Integer id);
    void saveQuestionTreeData(QuestionTreeCreateDto createDto);
    void deleteQuestionTreeData(Integer id);
    void updateQuestionTreeData(Integer id, QuestionTreeCreateDto updateDto);
}
