package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.JobPositionModel;

import java.util.List;
import java.util.Optional;

public interface JobPositionUseCase {

    List<JobPositionModel> findAllJobPosition();
    Optional<JobPositionModel> findJobPositionById(Integer jobPositionId);
    void saveJobPosition(JobPositionModel jobPositionModel);
    void deleteJobPositionById(Integer jobPositionId);
}
