package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.ImageDto;
import com.peacer.employeeassistantcenter.model.ImageModel;

import java.util.List;
import java.util.Optional;

public interface ImageUseCase {

    List<ImageDto> findAllImages();
    ImageDto findImageById(Integer imageId);
    void saveImage(ImageDto createDto);
    void deleteImageById(Integer imageId);
    void updateImage(Integer id, ImageDto updateDto);
}
