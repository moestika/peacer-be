package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.RoleDto;
import com.peacer.employeeassistantcenter.dto.RoleDetailDto;

import java.util.List;

public interface RoleUseCase {

    List<RoleDto> findAllRoles();
    RoleDetailDto findRoleById(Integer id);
    void createRoleData(RoleDto roleDto);
    void updateRoleData(Integer roleId, RoleDto roleDto);
    void deleteRoleData(Integer roleId);

}
