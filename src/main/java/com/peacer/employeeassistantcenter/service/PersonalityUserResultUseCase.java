package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.PersonalityUserResultCreateDto;
import com.peacer.employeeassistantcenter.dto.PersonalityUserResultViewDto;
import com.peacer.employeeassistantcenter.model.PersonalityUserResultModel;

import java.util.List;
import java.util.Optional;

public interface PersonalityUserResultUseCase {

    List<PersonalityUserResultViewDto> findAllPersonalityUserResults();
    PersonalityUserResultViewDto findPersonalityUserResultById(Integer userId, Integer resultId);
    void savePersonalityUserResult(PersonalityUserResultCreateDto createDto);
    void deletePersonalityUserResultById(Integer userResultId, Integer resultId);
    void updatePersonalityUserResult(Integer userId, Integer resultId, PersonalityUserResultCreateDto updateDto);
}
