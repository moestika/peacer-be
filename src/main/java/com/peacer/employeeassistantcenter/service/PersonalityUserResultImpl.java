package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PersonalityUserResultCreateDto;
import com.peacer.employeeassistantcenter.dto.PersonalityUserResultViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PersonalityResultModel;
import com.peacer.employeeassistantcenter.model.PersonalityUserResultModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.PersonalityResultRepository;
import com.peacer.employeeassistantcenter.repository.PersonalityUserResultRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonalityUserResultImpl implements PersonalityUserResultUseCase {

    @Autowired
    PersonalityUserResultRepository userResultRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PersonalityResultRepository resultRepository;

    @Override
    public List<PersonalityUserResultViewDto> findAllPersonalityUserResults()
    {
        List<PersonalityUserResultModel> model = userResultRepository.findAll();
        List<PersonalityUserResultViewDto> dtoList = new ArrayList<>();
        for (PersonalityUserResultModel var: model)
        {
            Optional<PersonalityResultModel> optResult = Optional.ofNullable(resultRepository.findById(var.getResult().getResultId()).orElseThrow(()->new ResourceNotFound("data not found")));
            if(optResult.isPresent())
            {
                Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(var.getUser().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
                if (optUser.isPresent())
                {
                    PersonalityUserResultViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, PersonalityUserResultViewDto.class);
                    dto.setId(var.getUserResultId());
                    dto.setUserName(var.getUser().getUserEmail());
                    dto.setResult(var.getResult().getResult());
                    dtoList.add(dto);
                }
            }
        }
        return dtoList;
    }

    @Override
    public PersonalityUserResultViewDto findPersonalityUserResultById(Integer userId, Integer resultId)
    {
        Optional<PersonalityResultModel> result = Optional.ofNullable(resultRepository.findById(resultId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> user = Optional.ofNullable(userRepository.findById(userId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<PersonalityUserResultModel> opt = Optional.ofNullable(userResultRepository.findByUserAndResult(user.get(), result.get()).orElseThrow(()->new ResourceNotFound("data not found")));
        PersonalityUserResultViewDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, PersonalityUserResultViewDto.class);
        dto.setId(opt.get().getUserResultId());
        dto.setUserName(user.get().getUserEmail());
        dto.setResult(result.get().getResult());
        return dto;
    }

    @Override
    public void savePersonalityUserResult(PersonalityUserResultCreateDto createDto)
    {
        PersonalityUserResultModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, PersonalityUserResultModel.class);
        Integer userId = createDto.getUserId();
        Integer resultId = createDto.getResultId();
        Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(userId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<PersonalityResultModel> optResult = Optional.ofNullable(resultRepository.findById(resultId).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setUserResultId(createDto.getUserResultId());
        payload.setUser(optUser.get());
        payload.setResult(optResult.get());
        userResultRepository.save(payload);
    }

    @Override
    public void deletePersonalityUserResultById(Integer userResultId, Integer resultId) {
        userResultRepository.deleteById(userResultId);
    }

    @Override
    public void updatePersonalityUserResult(Integer userId, Integer resultId, PersonalityUserResultCreateDto updateDto)
    {
        Optional<PersonalityResultModel> result = Optional.ofNullable(resultRepository.findById(resultId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> user = Optional.ofNullable(userRepository.findById(userId).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<PersonalityUserResultModel> opt = userResultRepository.findByUserAndResult(user.get(), result.get());
        if (opt.isPresent())
        {
            Optional<PersonalityResultModel> newResult = Optional.ofNullable(resultRepository.findById(updateDto.getResultId()).orElseThrow(()->new ResourceNotFound("data not found")));
            Optional<UserModel> newUser = Optional.ofNullable(userRepository.findById(updateDto.getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            PersonalityUserResultModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, PersonalityUserResultModel.class);
            model.setUserResultId(opt.get().getUserResultId());
            model.setUser(newUser.get());
            model.setResult(newResult.get());
            userResultRepository.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }

}
