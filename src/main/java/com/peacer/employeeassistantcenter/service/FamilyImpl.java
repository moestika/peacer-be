package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.FamilyDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.EmployeeModel;
import com.peacer.employeeassistantcenter.model.FamilyModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.EmployeeRepository;
import com.peacer.employeeassistantcenter.repository.FamilyRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FamilyImpl implements FamilyUseCase {

    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<FamilyDto> findAllFamilies() {
        List<FamilyModel> familyModels = familyRepository.findAll();
        List<FamilyDto> familyDtoList = new ArrayList<>();
        for (FamilyModel familyModel : familyModels) {
            FamilyDto familyDto = ModelMapperConfiguration.modelMapperConfig().map(familyModel,FamilyDto.class);
            familyDto.setEmployeeId(familyModel.getEmployee().getEmployeeId());
            familyDtoList.add(familyDto);
        }
        return familyDtoList;
    }

    @Override
    public List<FamilyDto> findAllFamiliesByUserId(Integer userId) {
        Optional<UserModel> userModelOptional = userRepository.findById(userId);
        if (userModelOptional.isPresent()) {
            Optional<EmployeeModel> employeeModelOptional = employeeRepository.findByUser(userModelOptional.get());
            if (employeeModelOptional.isPresent()) {
                List<FamilyModel> familyModels = familyRepository.findAllFamiliesByEmployee(employeeModelOptional.get());
                List<FamilyDto> familyDtoList = new ArrayList<>();
                for (FamilyModel familyModel : familyModels) {
                    FamilyDto familyDto = ModelMapperConfiguration.modelMapperConfig().map(familyModel,FamilyDto.class);
                    familyDto.setEmployeeId(familyModel.getEmployee().getEmployeeId());
                    familyDtoList.add(familyDto);
                }
                return familyDtoList;
            } else {
                throw new ResourceNotFound("Employee data not found with user: " + userId);
            }
        } else {
            throw new ResourceNotFound("User not found: " + userId);
        }
    }

    @Override
    public FamilyDto findFamilyById(Integer familyId) {
        Optional<FamilyModel> familyModelOptional = familyRepository.findById(familyId);
        if (familyModelOptional.isPresent()) {
            FamilyDto familyDto = ModelMapperConfiguration.modelMapperConfig().map(familyModelOptional.get(),FamilyDto.class);
            return familyDto;
        } else {
            throw new ResourceNotFound("Family data not found: " + familyId);
        }
    }

    @Override
    public void createFamilyData(FamilyDto familyDto) {
        Optional<EmployeeModel> employeeModelOptional = employeeRepository.findById(familyDto.getEmployeeId());
        if (employeeModelOptional.isPresent()) {
            List<FamilyModel> familyModelList = familyRepository.findAllFamiliesByEmployee(employeeModelOptional.get());
            for (FamilyModel familyModel : familyModelList) {
                if (familyModel.getFamilyStatus().equals("father") && familyDto.getFamilyStatus().equals("father")) {
                    throw new ResourceNotFound("Family status Father already exist");
                }
                if (familyModel.getFamilyStatus().equals("mother") && familyDto.getFamilyStatus().equals("mother")) {
                    throw new ResourceNotFound("Family status Mother already exist");
                }
            }
            FamilyModel familyModel = ModelMapperConfiguration.modelMapperConfig().map(familyDto,FamilyModel.class);
            familyModel.setEmployee(employeeModelOptional.get());
            familyRepository.save(familyModel);
        } else {
            throw new ResourceNotFound("Employee data not found: " + familyDto.getEmployeeId());
        }
    }

    @Override
    public FamilyDto updateFamilyData(Integer familyId, FamilyDto familyDto) {
        Optional<FamilyModel> familyModelOptional = familyRepository.findById(familyId);
        if (familyModelOptional.isPresent()) {
            Optional<EmployeeModel> employeeModelOptional = employeeRepository.findById(familyDto.getEmployeeId());
            if (employeeModelOptional.isPresent()) {
                FamilyModel familyModel = ModelMapperConfiguration.modelMapperConfig().map(familyDto,FamilyModel.class);
                familyModel.setFamilyId(familyId);
                familyModel.setEmployee(employeeModelOptional.get());
                familyRepository.save(familyModel);
                FamilyDto familyDtoReturn = ModelMapperConfiguration.modelMapperConfig().map(familyModel,FamilyDto.class);
                return familyDtoReturn;
            } else {
                throw new ResourceNotFound("Employee data not found: " + familyDto.getEmployeeId());
            }
        } else {
            throw new ResourceNotFound("Family data not found: " + familyId);
        }
    }

    @Override
    public void deleteFamilyDataById(Integer familyId) {
        Optional<FamilyModel> familyModelOptional = familyRepository.findById(familyId);
        if (familyModelOptional.isPresent()) {
            familyRepository.deleteById(familyId);
        } else {
            throw new ResourceNotFound("Family data not found: " + familyId);
        }
    }

}
