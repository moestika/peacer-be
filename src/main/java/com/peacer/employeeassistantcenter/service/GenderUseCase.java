package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.GenderModel;

import java.util.List;
import java.util.Optional;

public interface GenderUseCase {

    List<GenderModel> findAllGenders();
    Optional<GenderModel> findGenderById(Integer genderId);
    void saveGender(GenderModel genderModel);
    void deleteGenderById(Integer genderId);

}
