package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PersonalityAnswerDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PersonalityAnswerModel;
import com.peacer.employeeassistantcenter.model.PersonalityTestModel;
import com.peacer.employeeassistantcenter.repository.PersonalityAnswerRepository;
import com.peacer.employeeassistantcenter.repository.PersonalityTestRepository;
import io.swagger.models.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonalityAnswerImpl implements PersonalityAnswerUseCase
{
    @Autowired
    PersonalityAnswerRepository repo;
    @Autowired
    PersonalityTestRepository testRepository;

    @Override
    public List<PersonalityAnswerDto> findAllPersonalityAnswer()
    {
        List<PersonalityAnswerModel> modelList = repo.findAll();
        List<PersonalityAnswerDto> dtoList = new ArrayList<>();

        for (PersonalityAnswerModel var: modelList)
        {
            Optional<PersonalityTestModel> opt = testRepository.findById(var.getQuestion().getPersonalityTestId());
            if(opt.isPresent())
            {
                PersonalityAnswerDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, PersonalityAnswerDto.class);
                dto.setAnswerId(var.getAnswerId());
                dto.setAnswer(var.getAnswer());
                dto.setQuestionId(opt.get().getPersonalityTestId());
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    @Override
    public PersonalityAnswerDto findPersonalityAnswerById(Integer id)
    {
        Optional<PersonalityAnswerModel> opt = Optional.ofNullable(repo.findById(id).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<PersonalityTestModel> optQuestion = Optional.ofNullable(testRepository.findById(opt.get().getQuestion().getPersonalityTestId()).orElseThrow(()->new ResourceNotFound("data not found")));
        PersonalityAnswerDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, PersonalityAnswerDto.class);
        dto.setQuestionId(optQuestion.get().getPersonalityTestId());
        dto.setAnswerId(opt.get().getAnswerId());
        dto.setAnswer(opt.get().getAnswer());
        return dto;
    }

    @Override
    public void savePersonalityAnswerData(PersonalityAnswerDto createDto)
    {
        PersonalityAnswerModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, PersonalityAnswerModel.class);
        Integer questionId = createDto.getQuestionId();
        Optional<PersonalityTestModel> opt = Optional.ofNullable(testRepository.findById(questionId).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setAnswerId(createDto.getAnswerId());
        payload.setQuestion(opt.get());
        repo.save(payload);
    }

    @Override
    public void deletePersonalityAnswerData(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public void updatePersonalityAnswerData(Integer id, PersonalityAnswerDto updateDto)
    {
        Optional<PersonalityAnswerModel> opt = repo.findById(id);
        if (opt.isPresent())
        {
            PersonalityAnswerModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, PersonalityAnswerModel.class);
            model.setAnswerId(id);
            repo.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
