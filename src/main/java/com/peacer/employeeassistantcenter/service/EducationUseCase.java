package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.EducationModel;

import java.util.List;
import java.util.Optional;

public interface EducationUseCase
{
    List<EducationModel> findAllEducation();
    Optional<EducationModel> findEducationById(Integer id);
    void saveEducationData(EducationModel model);
    void deleteEducationData(EducationModel model);
}
