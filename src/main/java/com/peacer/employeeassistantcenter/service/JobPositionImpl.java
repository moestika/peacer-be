package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.model.JobPositionModel;
import com.peacer.employeeassistantcenter.repository.JobPositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JobPositionImpl implements JobPositionUseCase {

    @Autowired
    JobPositionRepository jobPositionRepository;

    @Override
    public List<JobPositionModel> findAllJobPosition() {
        return jobPositionRepository.findAll();
    }

    @Override
    public Optional<JobPositionModel> findJobPositionById(Integer jobPositionId) {
        return jobPositionRepository.findById(jobPositionId);
    }

    @Override
    public void saveJobPosition(JobPositionModel jobPositionModel) {
        jobPositionRepository.save(jobPositionModel);
    }

    @Override
    public void deleteJobPositionById(Integer jobPositionId) {
        jobPositionRepository.deleteById(jobPositionId);
    }

}
