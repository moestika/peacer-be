package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.FeedbackDto;
import com.peacer.employeeassistantcenter.model.FeedbackModel;

import java.util.List;
import java.util.Optional;

public interface FeedbackUseCase
{
    List<FeedbackDto> findAllFeedbacks();
    FeedbackDto findFeedbackById(Integer id);
    void saveFeedbackData(FeedbackDto createDto);
    void deleteFeedbackData(Integer id);
    void updateFeedbackData(Integer id, FeedbackDto updateDto);
}
