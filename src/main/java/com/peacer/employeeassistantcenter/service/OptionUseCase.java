package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.OptionDto;
import com.peacer.employeeassistantcenter.model.OptionModel;
import org.springframework.expression.spel.ast.OpDec;

import java.util.List;
import java.util.Optional;

public interface OptionUseCase
{
    List<OptionDto> findAllOptions();
    OptionDto findOptionById(Integer id);
    void saveOptionData(OptionDto createDto);
    void deleteOptionData(Integer id);
    void updateOptionData(Integer id, OptionDto updateDto);
}
