package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.FeedbackDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.FeedbackModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import com.peacer.employeeassistantcenter.repository.FeedbackRepository;
import com.peacer.employeeassistantcenter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FeedbackImpl implements FeedbackUseCase {

    @Autowired
    FeedbackRepository repo;
    @Autowired
    UserRepository userRepository;

    @Override
    public List<FeedbackDto> findAllFeedbacks()
    {
        List<FeedbackModel> models = repo.findAll();
        List<FeedbackDto> dtoList = new ArrayList<>();
        for(FeedbackModel var: models)
        {
            Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(var.getUser().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
            FeedbackDto dto = ModelMapperConfiguration.modelMapperConfig().map(var, FeedbackDto.class);
            dto.setCreatorUserId(optUser.get().getUserId());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public FeedbackDto findFeedbackById(Integer id)
    {
        Optional<FeedbackModel> opt = Optional.ofNullable(repo.findById(id).orElseThrow(()->new ResourceNotFound("data not found")));
        Optional<UserModel> optUser = Optional.ofNullable(userRepository.findById(opt.get().getUser().getUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        FeedbackDto dto = ModelMapperConfiguration.modelMapperConfig().map(opt, FeedbackDto.class);
        dto.setFeedbackContent(opt.get().getFeedbackContent());
        dto.setCreatorUserId(optUser.get().getUserId());
        return dto;
    }

    @Override
    @Transactional
    public void saveFeedbackData(FeedbackDto createDto)
    {
        FeedbackModel payload = ModelMapperConfiguration.modelMapperConfig().map(createDto, FeedbackModel.class);
        Optional<UserModel> opt = Optional.ofNullable(userRepository.findById(createDto.getCreatorUserId()).orElseThrow(()->new ResourceNotFound("data not found")));
        payload.setFeedbackId(null);
        payload.setUser(opt.get());
        repo.save(payload);
    }

    @Override
    public void deleteFeedbackData(Integer id) {
        repo.deleteById(id);
    }

    @Override
    public void updateFeedbackData(Integer id, FeedbackDto updateDto)
    {
        Optional<FeedbackModel> opt = repo.findById(id);
        if (opt.isPresent())
        {
            FeedbackModel model = ModelMapperConfiguration.modelMapperConfig().map(updateDto, FeedbackModel.class);
            model.setFeedbackId(id);
            repo.save(model);
        }
        else
        {
            throw new ResourceNotFound("Parent data not found");
        }
    }
}
