package com.peacer.employeeassistantcenter.service;

import com.peacer.employeeassistantcenter.dto.QuestionPoolCreateDto;
import com.peacer.employeeassistantcenter.dto.QuestionPoolDetailDto;
import com.peacer.employeeassistantcenter.dto.QuestionPoolViewDto;
import com.peacer.employeeassistantcenter.model.QuestionPoolModel;

import java.util.List;
import java.util.Optional;

public interface QuestionPoolUseCase {

    List<QuestionPoolViewDto> findAllQuestionPools();
    QuestionPoolDetailDto findQuestionPoolById(Integer questionPoolId);
    void saveQuestionPool(QuestionPoolCreateDto createDto);
    void deleteQuestionPoolById(Integer questionPoolId);
    void updateQuestionPool(Integer id, QuestionPoolCreateDto updateDto);
}
