package com.peacer.employeeassistantcenter.configurations;

import org.modelmapper.ModelMapper;

public class ModelMapperConfiguration {

    public static ModelMapper modelMapperConfig() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper;
    }

}
