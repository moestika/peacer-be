package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class FeedbackDto
{
    private String feedbackContent;
    private Integer creatorUserId;
}
