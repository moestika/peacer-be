package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class QuestionTreeCreateDto
{
    private Integer questionId;
    private String question;
    private Date questionDate;
    private Integer creatorUserId;
    private Integer optionParentId;
}
