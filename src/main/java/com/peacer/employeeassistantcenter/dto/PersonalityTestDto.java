package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PersonalityTestDto
{
    private Integer personalityTestId;
    private Date personalityTestDateCreated;
    private String personalityTestQuestion;
    private String personalityTestType;
    private Integer personalityTestAuthor;
}
