package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EmployeeUpdateDto {

    private String employeeNip;
    private String employeeFullName;
    private String employeeNickName;
    private String employeeCompanyName;
    private Date employeeBirthDate;
    private String employeeJobPosition;
    private String employeeJobLocation;
    private String employeeGender;
    private String employeePhoto;
    private String employeeStatus;
    private String employeeSocialIg;
    private String employeeSocialFb;
    private String employeeSocialTw;
    private String employeeSocialLi;

}
