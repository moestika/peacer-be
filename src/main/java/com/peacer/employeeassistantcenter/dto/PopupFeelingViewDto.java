package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PopupFeelingViewDto
{
    private Integer popupId;
    private Integer userId;
    private String userName;
    private String popupChosen;
    private Date popupDate;
}
