package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class QuestionTreeViewDto
{
    private Integer questionTreeId;
    private Integer creatorUserId;
    private String creatorUserName;
    private String question;
    private Integer optionId;
    private String optionParent;
    private Date questionDate;
}
