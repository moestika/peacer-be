package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.hibernate.id.IntegralDataTypeHolder;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ArticleCreateDto
{
    private Integer articleId;
    private String articleContent;
    private Date articleDate;
    private String articleStatus;
    private String articleTitle;
    private String articleType;
    private Integer categoryId;
    private Integer creatorUserId;
}
