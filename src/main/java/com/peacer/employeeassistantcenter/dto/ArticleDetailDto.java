package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ArticleDetailDto
{
    private Integer articleId;
    private String articleContent;
    private Date articleDate;
    private String articleStatus;
    private String articleTitle;
    private String articleType;
    private Integer categoryId;
    private String categoryName;
    private Integer creatorUserId;
    private String creatorUserName;
}
