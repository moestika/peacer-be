package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class QuestionPoolDetailDto
{
    private Integer questionPoolId;
    private Date questionDate;
    private String question;
    private String questionResponse;
    private String questionStatus;
    private String questionTicketNumber;
    private String questionTopic;
    private Integer questionCategoryId;
    private String questionCategory;
    private Integer creatorUserId;
    private String creatorUserName;
    private Integer moverUserId;
    private String moverUserName;
    private Integer responseUserId;
    private String responseUserName;
}
