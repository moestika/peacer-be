package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class FamilyDto {

    private Integer familyId;
    private Integer employeeId;
    private String familyStatus;
    private String familyName;
    private String familyBirthPlace;
    private Date familyBirthDate;
    private String familyAddress;
    private String familyLatestEducation;
    private String familyProfession;

}
