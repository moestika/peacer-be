package com.peacer.employeeassistantcenter.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CounselorDetailDto {

    private Integer userId;
    private String counselorId;
    private String counselorNik;
    private String counselorName;
    private String counselorCompany;
    private String counselorExperience;
    private String counselorLatestEducation;
    private String counselorContractStart;
    private String counselorContractEnd;
    private String counselorPhoto;
    private Double ratingScore;
    private List<CounselorRatingDto> ratings;

}
