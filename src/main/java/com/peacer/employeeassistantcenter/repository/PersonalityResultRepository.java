package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.PersonalityResultModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalityResultRepository extends JpaRepository<PersonalityResultModel,Integer> {
}
