package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.EducationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EducationRepository extends JpaRepository<EducationModel, Integer>
{
}
