package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.PopupFeelingModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PopupFeelingsRepository extends JpaRepository<PopupFeelingModel, Integer>
{
}
