package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.ImageModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<ImageModel,Integer> {
}
