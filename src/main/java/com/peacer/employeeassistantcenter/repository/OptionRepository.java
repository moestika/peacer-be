package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.OptionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionRepository extends JpaRepository<OptionModel, Integer>
{
}
