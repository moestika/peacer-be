package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.QuestionTreeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionTreeRepository extends JpaRepository<QuestionTreeModel, Integer>
{
}
