package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.PersonalityAnswerModel;
import com.peacer.employeeassistantcenter.model.PersonalityUserAnswerModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonalityUserAnswerRepository extends JpaRepository<PersonalityUserAnswerModel, Integer>
{
    Optional<PersonalityUserAnswerModel> findByUserAnswerAndUserPicks(PersonalityAnswerModel answerModel, UserModel userModel);
}
