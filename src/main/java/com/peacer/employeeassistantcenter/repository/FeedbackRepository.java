package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.FeedbackModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackRepository extends JpaRepository<FeedbackModel, Integer> {
}
