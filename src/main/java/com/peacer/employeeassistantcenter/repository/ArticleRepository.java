package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.ArticleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<ArticleModel,Integer> {
}
