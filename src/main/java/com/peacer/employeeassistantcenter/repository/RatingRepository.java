package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.CounselorModel;
import com.peacer.employeeassistantcenter.model.RatingModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<RatingModel, Integer> {

    List<RatingModel> findAllByCounselor(CounselorModel counselorModel);

}
