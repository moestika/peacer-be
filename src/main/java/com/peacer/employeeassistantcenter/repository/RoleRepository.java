package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel,Integer> {
    Optional<RoleModel> findByRoleName(String roleName);
}
