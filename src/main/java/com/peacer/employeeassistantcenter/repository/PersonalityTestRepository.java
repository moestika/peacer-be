package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.PersonalityTestModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalityTestRepository extends JpaRepository<PersonalityTestModel,Integer> {
}
