package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.CompanyModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyModel,Integer> {
}
