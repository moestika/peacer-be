package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {
    Optional<UserModel> findByUserEmail(String userEmail);
}
