package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.QuestionPoolModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionPoolRepository extends JpaRepository<QuestionPoolModel,Integer> {
}
