package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.EmployeeModel;
import com.peacer.employeeassistantcenter.model.FamilyModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FamilyRepository extends JpaRepository<FamilyModel, Integer> {

    List<FamilyModel> findAllFamiliesByEmployee(EmployeeModel employeeModel);

}
