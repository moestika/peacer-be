package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonalityUserResultRepository extends JpaRepository<PersonalityUserResultModel,Integer>
{
    Optional<PersonalityUserResultModel> findByUserAndResult(UserModel userModel, PersonalityResultModel resultModel);
}
