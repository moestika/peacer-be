package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.PersonalityAnswerModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalityAnswerRepository extends JpaRepository<PersonalityAnswerModel, Integer> {
}
