package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.GenderModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends JpaRepository<GenderModel,Integer> {
}
