package com.peacer.employeeassistantcenter.repository;

import com.peacer.employeeassistantcenter.model.EmployeeModel;
import com.peacer.employeeassistantcenter.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel, Integer> {

    Optional<EmployeeModel> findByUser(UserModel userModel);

}
