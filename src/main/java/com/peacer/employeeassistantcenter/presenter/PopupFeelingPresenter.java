package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.PopupFeelingCreateDto;
import com.peacer.employeeassistantcenter.dto.PopupFeelingViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PopupFeelingModel;
import com.peacer.employeeassistantcenter.service.PopupFeelingUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@Api(value = "Popup Feeling Management", description = "Presenter for popup feelings data to manage random popup")
public class PopupFeelingPresenter
{
    @Autowired
    PopupFeelingUseCase useCase;

    @PostMapping("popup-feeling")
    public Map<String, Object> createFeeling(@RequestBody PopupFeelingCreateDto createDto)
    {
        useCase.savePopupData(createDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        feedback.put("data", createDto);
        return feedback;
    }

    @GetMapping("popup-feelings")
    public Map<String, Object> getFeelings()
    {
        List<PopupFeelingViewDto> list = useCase.findAllPopup();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("popup-feeling/{id}")
    public Map<String, Object> getFeeling(@PathVariable Integer id)
    {
        PopupFeelingViewDto dto = useCase.findPopupById(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", dto);
        return feedback;
    }

    @PutMapping("popup-feeling/{id}")
    public Map<String, Object> updateFeeling(@PathVariable Integer id, @RequestBody PopupFeelingCreateDto updateDto)
    {
        useCase.updatePopupData(id, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("popup-feeling/{id}")
    public Map<String, Object> deleteFeeling(@PathVariable Integer id)
    {
        useCase.deletePopupData(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
