package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.model.PersonalityResultModel;
import com.peacer.employeeassistantcenter.service.PersonalityResultUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class PersonalityResultPresenter {
    
    @Autowired
    PersonalityResultUseCase personalityResultUseCase;

    @GetMapping("personality-results")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllPersonalityResults() {
        Map<String,Object> map = new HashMap<>();
        List<PersonalityResultModel> personalityResultModelList = personalityResultUseCase.findAllPersonalityResults();
        map.put("message","Command completed successfully");
        map.put("total",personalityResultModelList.size());
        map.put("data",personalityResultModelList);
        return map;
    }

    @PostMapping("personality-result")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createPersonalityResult(@Valid @RequestBody PersonalityResultModel personalityResultModel) {
        Map<String,Object> map = new HashMap<>();
        personalityResultUseCase.savePersonalityResult(personalityResultModel);
        map.put("message","PersonalityResult created successfully");
        map.put("data", personalityResultModel);
        return map;
    }

    @PutMapping("personality-result/{personalityResultId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updatePersonalityResult(@PathVariable Integer personalityResultId, @Valid @RequestBody PersonalityResultModel personalityResultModel) {
        Map<String,Object> map = new HashMap<>();
        Optional<PersonalityResultModel> optionalPersonalityResultModel = personalityResultUseCase.findPersonalityResultById(personalityResultId);
        optionalPersonalityResultModel.get().setResult(personalityResultModel.getResult());
        optionalPersonalityResultModel.get().setResultDesc(personalityResultModel.getResultDesc());
        personalityResultUseCase.savePersonalityResult(optionalPersonalityResultModel.get());
        map.put("message","PersonalityResult updated successfully");
        map.put("data", optionalPersonalityResultModel.get());
        return map;
    }

    @DeleteMapping("personality-result/{personalityResultId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deletePersonalityResult(@PathVariable Integer personalityResultId) {
        Map<String,Object> map = new HashMap<>();
        personalityResultUseCase.deletePersonalityResultById(personalityResultId);
        map.put("message","PersonalityResult deleted successfully");
        return map;
    }
    
}
