package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.PersonalityTestDto;
import com.peacer.employeeassistantcenter.service.PersonalityTestUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/")
public class PersonalityTestPresenter {

    @Autowired
    PersonalityTestUseCase personalityTestUseCase;

    @GetMapping("personality-tests")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllPersonalityTests() {
        List<PersonalityTestDto> personalityTestModelList = personalityTestUseCase.findAllPersonalityTests();
        Map<String,Object> map = new HashMap<>();
        map.put("message","Command completed successfully");
        map.put("total",personalityTestModelList.size());
        map.put("data",personalityTestModelList);
        return map;
    }

    @GetMapping("personality-test/{personalityTestId}")
    public Map<String, Object> findPersonalityTest(@PathVariable Integer personalityTestId) {
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "Command completed successfully");
        feedback.put("data", personalityTestUseCase.findPersonalityTestById(personalityTestId));
        return feedback;
    }

    @PostMapping("personality-test")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createPersonalityTest(@Valid @RequestBody PersonalityTestDto personalityTestDto) {
        Map<String,Object> map = new HashMap<>();
        map.put("message","PersonalityTest created successfully");
        map.put("data", personalityTestUseCase.createPersonalityTest(personalityTestDto));
        return map;
    }

    @PutMapping("personality-test/{personalityTestId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updatePersonalityTest(@PathVariable Integer personalityTestId, @Valid @RequestBody PersonalityTestDto personalityTestDto) {
        Map<String,Object> map = new HashMap<>();
        map.put("message","PersonalityTest updated successfully");
        map.put("data", personalityTestUseCase.updatePersonalityTest(personalityTestId, personalityTestDto));
        return map;
    }

    @DeleteMapping("personality-test/{personalityTestId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deletePersonalityTest(@PathVariable Integer personalityTestId) {
        Map<String,Object> map = new HashMap<>();
        personalityTestUseCase.deletePersonalityTestById(personalityTestId);
        map.put("message","PersonalityTest deleted successfully");
        return map;
    }
    
}
