package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PersonalityUserAnswerCreateDto;
import com.peacer.employeeassistantcenter.dto.PersonalityUserAnswerViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PersonalityUserAnswerModel;
import com.peacer.employeeassistantcenter.service.PersonalityUserAnswerUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
public class PersonalityUserAnswerPresenter
{
    @Autowired
    PersonalityUserAnswerUseCase useCase;

    @PostMapping("personality-user-answer")
    public Map<String, Object> createAnswer(@RequestBody PersonalityUserAnswerCreateDto createDto)
    {
        useCase.savePersonalityUserAnswer(createDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        feedback.put("data", createDto);
        return feedback;
    }

    @GetMapping("personality-user-answers")
    public Map<String, Object> getAnswers()
    {
        List<PersonalityUserAnswerViewDto> list = useCase.findAllPersonalityUserAnswers();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("personality-user-answer/{answerid}/{userid}")
    public Map<String, Object> getAnswer(@PathVariable Integer answerid, @PathVariable Integer userid)
    {
        PersonalityUserAnswerViewDto dto = useCase.findPersonalityUserAnswersById(answerid, userid);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", dto);
        return feedback;
    }

    @PutMapping("personality-user-answer/{answerid}/{userid}")
    public Map<String, Object> updateAnswer(@PathVariable Integer answerid, @PathVariable Integer userid, @RequestBody PersonalityUserAnswerCreateDto updateDto)
    {
        useCase.updatePersonalityUserAnswer(answerid, userid, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("personality-user-answer/{answerid}/{userid}")
    public Map<String, Object> deleteAnswer(@PathVariable Integer answerid, @PathVariable Integer userid)
    {
        PersonalityUserAnswerViewDto dto = useCase.findPersonalityUserAnswersById(answerid,userid);
        useCase.deletePersonalityUserAnswers(dto.getId());
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
