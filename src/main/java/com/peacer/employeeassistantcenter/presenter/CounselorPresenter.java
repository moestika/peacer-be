package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.CounselorAllDto;
import com.peacer.employeeassistantcenter.dto.CounselorCreateDto;
import com.peacer.employeeassistantcenter.dto.CounselorUpdateDto;
import com.peacer.employeeassistantcenter.service.CounselorUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/")
@Api(value = "Counselor Management", description = "Presenter for counselor data related to user model")
public class CounselorPresenter {
    
    @Autowired
    CounselorUseCase useCase;

    @PostMapping("counselor")
    public Map<String, Object> createCounselor(@RequestBody CounselorCreateDto counselorCreateDto) {
        useCase.createCounselorData(counselorCreateDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Counselor Data created successfully");
        map.put("data", counselorCreateDto);
        return map;
    }

    @GetMapping("counselors")
    public Map<String, Object> getCounselors() {
        List<CounselorAllDto> list = useCase.findAllCounselors();
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("total", list.size());
        map.put("data", list);
        return map;
    }

    @GetMapping("counselor/{userId}")
    public Map<String, Object> getCounselor(@PathVariable Integer userId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", useCase.findCounselorByUserId(userId));
        return map;
    }

    @PutMapping("counselor/{userId}")
    public Map<String, Object> updateCounselor(@PathVariable Integer userId, @RequestBody CounselorUpdateDto counselorUpdateDto) {
        useCase.updateCounselorData(userId,counselorUpdateDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", counselorUpdateDto);
        return map;
    }

    @DeleteMapping("counselor/{userId}")
    public Map<String, Object> deleteCounselor(@PathVariable Integer userId) {
        useCase.deleteCounselorData(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Counselor Data deleted successfully");
        return map;
    }

}
