package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.ImageDto;
import com.peacer.employeeassistantcenter.model.ImageModel;
import com.peacer.employeeassistantcenter.service.ImageUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@Api(value = "Image Management",description = "Presenter for image data related to article model")
public class ImagePresenter {

    @Autowired
    ImageUseCase imageUseCase;

    @GetMapping("images")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllImages() {
        Map<String,Object> map = new HashMap<>();
        List<ImageDto> imageModelList = imageUseCase.findAllImages();
        map.put("message","Command completed successfully");
        map.put("total",imageModelList.size());
        map.put("data",imageModelList);
        return map;
    }

    @GetMapping("image/{imageId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findImageById(@PathVariable Integer imageId) {
        Map<String,Object> map = new HashMap<>();
        ImageDto dto = imageUseCase.findImageById(imageId);
        map.put("message","Command completed successfully");
        map.put("data",dto);
        return map;
    }

    @PostMapping("image")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createImage(@Valid @RequestBody ImageDto createDto) {
        Map<String,Object> map = new HashMap<>();
        imageUseCase.saveImage(createDto);
        map.put("message","Command completed successfully");
        map.put("data",createDto);
        return map;
    }

    @PutMapping("image/{imageId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateImageById(@PathVariable Integer imageId, @Valid @RequestBody ImageDto updateDto) {
        Map<String,Object> map = new HashMap<>();
        imageUseCase.updateImage(imageId, updateDto);
        map.put("message","Image updated successfully");
        map.put("data",updateDto);
        return map;
    }

    @DeleteMapping("image/{imageId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteImageById(@PathVariable Integer imageId) {
        Map<String,Object> map = new HashMap<>();
        imageUseCase.deleteImageById(imageId);
        map.put("message","Image deleted successfully");
        return map;
    }
    
}
