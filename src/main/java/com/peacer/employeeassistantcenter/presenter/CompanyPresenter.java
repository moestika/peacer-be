package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.model.CompanyModel;
import com.peacer.employeeassistantcenter.service.CompanyUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class CompanyPresenter {

    @Autowired
    CompanyUseCase companyUseCase;

    @GetMapping("companies")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllCompanys() {
        Map<String,Object> map = new HashMap<>();
        List<CompanyModel> companyModelList = companyUseCase.findAllCompanies();
        map.put("message","Command completed successfully");
        map.put("total",companyModelList.size());
        map.put("data",companyModelList);
        return map;
    }

    @PostMapping("company")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createCompany(@Valid @RequestBody CompanyModel companyModel) {
        Map<String,Object> map = new HashMap<>();
        companyUseCase.saveCompany(companyModel);
        map.put("message","Company created successfully");
        map.put("data", companyModel);
        return map;
    }

    @PutMapping("company/{companyId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateCompany(@PathVariable Integer companyId, @Valid @RequestBody CompanyModel companyModel) {
        Map<String,Object> map = new HashMap<>();
        Optional<CompanyModel> optionalCompanyModel = companyUseCase.findCompanyById(companyId);
        optionalCompanyModel.get().setCompanyName(companyModel.getCompanyName());
        companyUseCase.saveCompany(optionalCompanyModel.get());
        map.put("message","Company updated successfully");
        map.put("data", optionalCompanyModel.get());
        return map;
    }

    @DeleteMapping("company/{companyId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteCompany(@PathVariable Integer companyId) {
        Map<String,Object> map = new HashMap<>();
        companyUseCase.deleteCompanyById(companyId);
        map.put("message","Company deleted successfully");
        return map;
    }
    
}
