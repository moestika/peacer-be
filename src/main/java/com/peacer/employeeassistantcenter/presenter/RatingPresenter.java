package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.RatingDto;
import com.peacer.employeeassistantcenter.service.RatingUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/")
@Api(value = "Rating Management")
public class RatingPresenter {
    
    @Autowired
    RatingUseCase useCase;

    @PostMapping("rating")
    public Map<String, Object> createRating(@RequestBody RatingDto ratingDto) {
        useCase.createRatingData(ratingDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Rating data created successfully");
        map.put("data", ratingDto);
        return map;
    }

    @GetMapping("ratings")
    public Map<String, Object> getRatings() {
        List<RatingDto> list = useCase.findAllRatings();
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("total", list.size());
        map.put("data", list);
        return map;
    }

    @GetMapping("rating/{ratingId}")
    public Map<String, Object> getRating(@PathVariable Integer ratingId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", useCase.findRatingById(ratingId));
        return map;
    }

    @PutMapping("rating/{ratingId}")
    public Map<String, Object> updateRating(@PathVariable Integer ratingId, @RequestBody RatingDto ratingDto) {
        useCase.updateRatingData(ratingId,ratingDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Rating data updated successfully");
        map.put("data", ratingDto);
        return map;
    }

    @DeleteMapping("rating/{ratingId}")
    public Map<String, Object> deleteRating(@PathVariable Integer ratingId) {
        useCase.deleteRatingData(ratingId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Rating data deleted successfully");
        return map;
    }

}
