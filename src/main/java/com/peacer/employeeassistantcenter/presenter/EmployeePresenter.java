package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.EmployeeAllDto;
import com.peacer.employeeassistantcenter.dto.EmployeeCreateDto;
import com.peacer.employeeassistantcenter.dto.EmployeeUpdateDto;
import com.peacer.employeeassistantcenter.service.EmployeeUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/")
@Api(value = "Employee Management", description = "Presenter for employee data related to user model")
public class EmployeePresenter {

    @Autowired
    EmployeeUseCase useCase;

    @PostMapping("employee")
    public Map<String, Object> createEmployee(@RequestBody EmployeeCreateDto employeeCreateDto) {
        useCase.createEmployeeData(employeeCreateDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message","Employee Data created successfully");
        map.put("data",employeeCreateDto);
        return map;
    }

    @GetMapping("employees")
    public Map<String, Object> getEmployees() {
        List<EmployeeAllDto> employeeAllDtoList = useCase.findAllEmployees();
        Map<String, Object> map = new HashMap<>();
        map.put("message","Command completed successfully");
        map.put("total", employeeAllDtoList.size());
        map.put("data", employeeAllDtoList);
        return map;
    }

    @GetMapping("employee/{userId}")
    public Map<String, Object> getEmployee(@PathVariable Integer userId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", useCase.findEmployeeByUserId(userId));
        return map;
    }

    @PutMapping("employee/{userId}")
    public Map<String, Object> updateEmployee(@PathVariable Integer userId, @RequestBody EmployeeUpdateDto employeeUpdateDto) {
        useCase.updateEmployeeData(userId,employeeUpdateDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Employee Data updated successfully");
        map.put("data",employeeUpdateDto);
        return map;
    }

    @DeleteMapping("employee/{userId}")
    public Map<String, Object> deleteEmployee(@PathVariable Integer userId) {
        useCase.deleteEmployeeData(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Employee Data deleted successfully");
        return map;
    }

}
