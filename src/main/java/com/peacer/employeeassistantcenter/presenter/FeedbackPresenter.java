package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.FeedbackDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.FeedbackModel;
import com.peacer.employeeassistantcenter.service.FeedbackUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
@Api(value = "Feedback Management", description = "Presenter for feedback data created by user after using LEA")
public class FeedbackPresenter
{
    @Autowired
    FeedbackUseCase useCase;

    @PostMapping("feedback")
    public Map<String, Object> createFeedback(@RequestBody FeedbackDto createDto)
    {
        useCase.saveFeedbackData(createDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        feedback.put("data", createDto);
        return feedback;
    }

    @GetMapping("feedbacks")
    public Map<String, Object> getFeedbacks()
    {
        List<FeedbackDto> list = useCase.findAllFeedbacks();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("feedback/{id}")
    public Map<String, Object> getFeedback(@PathVariable Integer id)
    {
        FeedbackDto dto = useCase.findFeedbackById(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", dto);
        return feedback;
    }

    @PutMapping("feedback/{id}")
    public Map<String, Object> updateFeedback(@PathVariable Integer id, @RequestBody FeedbackDto updateDto)
    {
        useCase.updateFeedbackData(id, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("feedback/{id}")
    public Map<String, Object> deleteFeedback(@PathVariable Integer id)
    {
        useCase.deleteFeedbackData(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
