package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.ArticleCreateDto;
import com.peacer.employeeassistantcenter.dto.ArticleDetailDto;
import com.peacer.employeeassistantcenter.dto.ArticleViewDto;
import com.peacer.employeeassistantcenter.model.ArticleModel;
import com.peacer.employeeassistantcenter.service.ArticleUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@Api(value = "Articles Management",description = "Presenter for article data to manage content in end user interface")
public class ArticlePresenter {

    @Autowired
    ArticleUseCase articleUseCase;

    @GetMapping("articles")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllArticles() {
        Map<String,Object> map = new HashMap<>();
        List<ArticleViewDto> articleModelList = articleUseCase.findAllArticles();
        map.put("message","Command completed successfully");
        map.put("total",articleModelList.size());
        map.put("data",articleModelList);
        return map;
    }

    @GetMapping("article/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findArticleById(@PathVariable Integer articleId) {
        Map<String,Object> map = new HashMap<>();
        ArticleDetailDto dto = articleUseCase.findArticleById(articleId);
        map.put("message","Command completed successfully");
        map.put("data", dto);
        return map;
    }

    @PostMapping("article")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createArticle(@Valid @RequestBody ArticleCreateDto createDto) {
        Map<String,Object> map = new HashMap<>();
        articleUseCase.saveArticle(createDto);
        map.put("message","Article created successfully");
        map.put("data", createDto);
        return map;
    }

    @PutMapping("article/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateArticle(@PathVariable Integer articleId, @Valid @RequestBody ArticleCreateDto updateDto) {
        Map<String,Object> map = new HashMap<>();
        articleUseCase.updateArticleData(articleId,updateDto);
        map.put("message","Article updated successfully");
        map.put("data", updateDto);
        return map;
    }

    @DeleteMapping("article/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteArticle(@PathVariable Integer articleId) {
        Map<String,Object> map = new HashMap<>();
        articleUseCase.deleteArticleById(articleId);
        map.put("message","Article deleted successfully");
        return map;
    }

}
