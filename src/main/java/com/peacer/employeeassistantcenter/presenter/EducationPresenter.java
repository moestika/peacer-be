package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.EducationModel;
import com.peacer.employeeassistantcenter.service.EducationUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
@Api(value = " Education Management", description = "")
public class EducationPresenter
{
    @Autowired
    EducationUseCase useCase;

    @PostMapping("education")
    public Map<String, Object> createDegree(@RequestBody EducationModel model)
    {
        useCase.saveEducationData(model);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        feedback.put("data", model);
        return feedback;
    }

    @GetMapping("educations")
    public Map<String, Object> getDegrees()
    {
        List<EducationModel> list = useCase.findAllEducation();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("education/{id}")
    public Map<String, Object> getDegree(@PathVariable Integer id)
    {
        Optional<EducationModel> opt = Optional.ofNullable(useCase.findEducationById(id).orElseThrow(()-> new ResourceNotFound("data not found")));
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", opt.get());
        return feedback;
    }

    @PutMapping("education/{id}")
    public Map<String, Object> updateDegree(@PathVariable Integer id, @RequestBody EducationModel model)
    {
        Optional<EducationModel> opt = Optional.ofNullable(useCase.findEducationById(id).orElseThrow(()-> new ResourceNotFound("data not found")));
        opt.get().setEducationDegree(model.getEducationDegree());
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("education/{id}")
    public Map<String, Object> deleteDegree(@PathVariable Integer id)
    {
        Optional<EducationModel> opt = Optional.ofNullable(useCase.findEducationById(id).orElseThrow(()-> new ResourceNotFound("data not found")));
        useCase.deleteEducationData(opt.get());
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
