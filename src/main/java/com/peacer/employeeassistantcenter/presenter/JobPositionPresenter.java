package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.model.JobPositionModel;
import com.peacer.employeeassistantcenter.service.JobPositionUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class JobPositionPresenter {

    @Autowired
    JobPositionUseCase jobPositionUseCase;

    @GetMapping("job-positions")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllJobPositions() {
        Map<String,Object> map = new HashMap<>();
        List<JobPositionModel> jobPositionModelList = jobPositionUseCase.findAllJobPosition();
        map.put("message","Command completed successfully");
        map.put("total",jobPositionModelList.size());
        map.put("data",jobPositionModelList);
        return map;
    }

    @PostMapping("job-position")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createJobPosition(@Valid @RequestBody JobPositionModel jobPositionModel) {
        Map<String,Object> map = new HashMap<>();
        jobPositionUseCase.saveJobPosition(jobPositionModel);
        map.put("message","JobPosition created successfully");
        map.put("data", jobPositionModel);
        return map;
    }

    @PutMapping("job-position/{jobPositionId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateJobPosition(@PathVariable Integer jobPositionId, @Valid @RequestBody JobPositionModel jobPositionModel) {
        Map<String,Object> map = new HashMap<>();
        Optional<JobPositionModel> optionalJobPositionModel = jobPositionUseCase.findJobPositionById(jobPositionId);
        optionalJobPositionModel.get().setJobPositionName(jobPositionModel.getJobPositionName());
        jobPositionUseCase.saveJobPosition(optionalJobPositionModel.get());
        map.put("message","JobPosition updated successfully");
        map.put("data", optionalJobPositionModel.get());
        return map;
    }

    @DeleteMapping("job-position/{jobPositionId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteJobPosition(@PathVariable Integer jobPositionId) {
        Map<String,Object> map = new HashMap<>();
        jobPositionUseCase.deleteJobPositionById(jobPositionId);
        map.put("message","JobPosition deleted successfully");
        return map;
    }

}
