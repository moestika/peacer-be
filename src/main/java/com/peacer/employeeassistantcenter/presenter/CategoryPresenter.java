package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.CategoryDto;
import com.peacer.employeeassistantcenter.model.CategoryModel;
import com.peacer.employeeassistantcenter.service.CategoryUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@Api(value = "Category Management",description = "Presenter for category data related to topic and article")
public class CategoryPresenter {

    @Autowired
    CategoryUseCase categoryUseCase;

    @GetMapping("categories")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllCategories() {
        Map<String,Object> map = new HashMap<>();
        List<CategoryDto> categoryModelList = categoryUseCase.findAllCategories();
        map.put("message","Command completed successfully");
        map.put("total",categoryModelList.size());
        map.put("data",categoryModelList);
        return map;
    }

    @GetMapping("category/{categoryId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findCategoryById(@PathVariable Integer categoryId) {
        Map<String,Object> map = new HashMap<>();
        CategoryDto dto = categoryUseCase.findCategoryById(categoryId);
        map.put("message","Command completed successfully");
        map.put("data", dto);
        return map;
    }

    @PostMapping("category")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createCategory(@Valid @RequestBody CategoryDto createDto) {
        Map<String,Object> map = new HashMap<>();
        categoryUseCase.saveCategory(createDto);
        map.put("message","Command completed successfully");
        map.put("data",createDto);
        return map;
    }

    @PutMapping("category/{categoryId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateCategoryById(@PathVariable Integer categoryId, @Valid @RequestBody CategoryDto updateDto) {
        Map<String,Object> map = new HashMap<>();
        categoryUseCase.updateCategoryData(categoryId, updateDto);
        map.put("message","Category updated successfully");
        map.put("data", updateDto);
        return map;
    }

    @DeleteMapping("category/{categoryId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteCategoryById(@PathVariable Integer categoryId) {
        Map<String,Object> map = new HashMap<>();
        categoryUseCase.deleteCategoryById(categoryId);
        map.put("message","Category deleted successfully");
        return map;
    }

}
