package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.QuestionTreeCreateDto;
import com.peacer.employeeassistantcenter.dto.QuestionTreeViewDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.QuestionTreeModel;
import com.peacer.employeeassistantcenter.service.QuestionTreeUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
@Api(value = "Question Tree Data Management", description = "Presenter for Question Tree to manage question related to answer model")
public class QuestionTreePresenter
{
    @Autowired
    QuestionTreeUseCase useCase;

    @PostMapping("question-tree")
    public Map<String, Object> createQuestionTree(@RequestBody QuestionTreeCreateDto createDto)
    {
        useCase.saveQuestionTreeData(createDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        feedback.put("data", createDto);
        return feedback;
    }

    @GetMapping("question-trees")
    public Map<String, Object> getQuestionTrees()
    {
        List<QuestionTreeViewDto> list = useCase.findAllQuestionTree();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("question-tree/{id}")
    public Map<String, Object> getQuestionTree(@PathVariable Integer id)
    {
        QuestionTreeViewDto dto = useCase.findQuestionTreeById(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", dto);
        return feedback;
    }

    @PutMapping("question-tree/{id}")
    public Map<String, Object> updateQuestionTree(@PathVariable Integer id, @RequestBody QuestionTreeCreateDto updateDto)
    {
        useCase.updateQuestionTreeData(id, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("question-tree/{id}")
    public Map<String, Object> deleteQuestionTree(@PathVariable Integer id)
    {
        useCase.deleteQuestionTreeData(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
