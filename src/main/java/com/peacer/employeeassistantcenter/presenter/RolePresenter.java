package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.RoleDto;
import com.peacer.employeeassistantcenter.service.RoleUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/")
@Api(value = "Role Management", description = "Presenter for role data related to user model, to define authorization")
public class RolePresenter {
    
    @Autowired
    RoleUseCase useCase;

    @GetMapping("roles")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getRoles() {
        List<RoleDto> roleAllDtoList = useCase.findAllRoles();
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("total", roleAllDtoList.size());
        map.put("data", roleAllDtoList);
        return map;
    }

    @GetMapping("role/{roleId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getRole(@PathVariable Integer roleId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", useCase.findRoleById(roleId));
        return map;
    }

    @PostMapping("role")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createRole(@RequestBody RoleDto roleDto) {
        useCase.createRoleData(roleDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Role created successfully");
        map.put("data", roleDto);
        return map;
    }

    @PutMapping("role/{roleId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> updateRole(@PathVariable Integer roleId, @RequestBody RoleDto roleDto) {
        useCase.updateRoleData(roleId,roleDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Role updated successfully");
        map.put("data", roleDto);
        return map;
    }

    @DeleteMapping("role/{roleId}")
    public Map<String, Object> deleteRole(@PathVariable Integer roleId) {
        useCase.deleteRoleData(roleId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Role deleted successfully");
        return map;
    }

}
