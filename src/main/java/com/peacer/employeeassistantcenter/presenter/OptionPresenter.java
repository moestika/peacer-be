package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.OptionDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.OptionModel;
import com.peacer.employeeassistantcenter.service.OptionUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
public class OptionPresenter
{
    @Autowired
    OptionUseCase useCase;

    @PostMapping("option")
    public Map<String, Object> createOption(@RequestBody OptionDto createDto)
    {
        useCase.saveOptionData(createDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        return feedback;
    }

    @GetMapping("options")
    public Map<String, Object> getOptions()
    {
        List<OptionDto> list = useCase.findAllOptions();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("option/{id}")
    public Map<String, Object> getOption(@PathVariable Integer id)
    {
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", useCase.findOptionById(id));
        return feedback;
    }

    @PutMapping("option/{id}")
    public Map<String, Object> updateOption(@PathVariable Integer id, @RequestBody OptionDto updateDto)
    {
        useCase.updateOptionData(id, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("option/{id}")
    public Map<String, Object> deleteOption(@PathVariable Integer id)
    {
        useCase.deleteOptionData(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
