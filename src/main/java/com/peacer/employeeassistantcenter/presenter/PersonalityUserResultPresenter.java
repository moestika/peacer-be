package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.configurations.ModelMapperConfiguration;
import com.peacer.employeeassistantcenter.dto.PersonalityUserResultCreateDto;
import com.peacer.employeeassistantcenter.dto.PersonalityUserResultViewDto;
import com.peacer.employeeassistantcenter.model.PersonalityUserResultModel;
import com.peacer.employeeassistantcenter.service.PersonalityUserResultUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@Api(value = "User Personality Result Management")
public class PersonalityUserResultPresenter {

    @Autowired
    PersonalityUserResultUseCase userResultUseCase;

    @GetMapping("personality-user-results")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllPersonalityUserResults() {
        Map<String,Object> map = new HashMap<>();
        List<PersonalityUserResultViewDto> list = userResultUseCase.findAllPersonalityUserResults();
        map.put("message","Command completed successfully");
        map.put("total",list.size());
        map.put("data",list);
        return map;
    }

    @GetMapping("personality-user-result/{userId}/{resultId}")
    public Map<String, Object> findPersonalityUserResult(@PathVariable Integer userId, @PathVariable Integer resultId)
    {
        PersonalityUserResultViewDto dto = userResultUseCase.findPersonalityUserResultById(userId, resultId);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", dto);
        return feedback;
    }

    @PostMapping("personality-user-result")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createPersonalityUserResult(@Valid @RequestBody PersonalityUserResultCreateDto createDto) {
        Map<String,Object> map = new HashMap<>();
        userResultUseCase.savePersonalityUserResult(createDto);
        map.put("message","PersonalityUserResult created successfully");
        map.put("data", createDto);
        return map;
    }

    @PutMapping("personality-user-result/{userId}/{resultId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updatePersonalityUserResult(@PathVariable Integer userId, @PathVariable Integer resultId, @Valid @RequestBody PersonalityUserResultCreateDto updateDto)
    {
        userResultUseCase.updatePersonalityUserResult(userId, resultId, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data updated");
        return feedback;
    }

    @DeleteMapping("personality-user-result/{userId}/{resultId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deletePersonalityUserResult(@PathVariable Integer userId, @PathVariable Integer resultId)
    {
        PersonalityUserResultViewDto dto = userResultUseCase.findPersonalityUserResultById(userId, resultId);
        Map<String,Object> map = new HashMap<>();
        userResultUseCase.deletePersonalityUserResultById(userId, resultId);
        map.put("message","PersonalityUserResult deleted successfully");
        return map;
    }

}
