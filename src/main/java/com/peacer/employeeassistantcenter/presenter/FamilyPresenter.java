package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.FamilyDto;
import com.peacer.employeeassistantcenter.service.FamilyUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/")
public class FamilyPresenter {
    
    @Autowired
    FamilyUseCase useCase;

    @GetMapping("families")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getFamilies() {
        List<FamilyDto> list = useCase.findAllFamilies();
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("total", list.size());
        map.put("data", list);
        return map;
    }

    @GetMapping("families/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getFamiliesByUser(@PathVariable Integer userId) {
        List<FamilyDto> list = useCase.findAllFamiliesByUserId(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("total", list.size());
        map.put("data", list);
        return map;
    }

    @GetMapping("family/{familyId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getFamily(@PathVariable Integer familyId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", useCase.findFamilyById(familyId));
        return map;
    }

    @PostMapping("family")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createFamily(@RequestBody FamilyDto familyDto) {
        useCase.createFamilyData(familyDto);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Family data created successfully");
        map.put("data", familyDto);
        return map;
    }

    @PutMapping("family/{familyId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> updateFamily(@PathVariable Integer familyId, @RequestBody FamilyDto familyDto) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Family data updated successfully");
        map.put("data", useCase.updateFamilyData(familyId,familyDto));
        return map;
    }

    @DeleteMapping("family/{familyId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> deleteFamily(@PathVariable Integer familyId) {
        useCase.deleteFamilyDataById(familyId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Family data deleted successfully");
        return map;
    }
    
}
