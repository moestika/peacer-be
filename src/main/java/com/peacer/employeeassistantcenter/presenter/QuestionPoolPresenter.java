package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.QuestionPoolCreateDto;
import com.peacer.employeeassistantcenter.dto.QuestionPoolDetailDto;
import com.peacer.employeeassistantcenter.dto.QuestionPoolViewDto;
import com.peacer.employeeassistantcenter.model.QuestionPoolModel;
import com.peacer.employeeassistantcenter.service.QuestionPoolUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
@Api(value = "Question Pool Management",description = "Presenter for question data to manage question and answer for employee to counselor")
public class QuestionPoolPresenter {

    @Autowired
    QuestionPoolUseCase questionPoolUseCase;

    @GetMapping("question-pools")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllCategories() {
        Map<String,Object> map = new HashMap<>();
        List<QuestionPoolViewDto> dto = questionPoolUseCase.findAllQuestionPools();
        map.put("message","Command completed successfully");
        map.put("total", dto.size());
        map.put("data", dto);
        return map;
    }

    @GetMapping("question-pool/{questionPoolId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findQuestionPoolById(@PathVariable Integer questionPoolId) {
        Map<String,Object> map = new HashMap<>();
        QuestionPoolDetailDto dto = questionPoolUseCase.findQuestionPoolById(questionPoolId);
        map.put("message","Command completed successfully");
        map.put("data", dto);
        return map;
    }

    @PostMapping("question-pool")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createQuestionPool(@Valid @RequestBody QuestionPoolCreateDto createDto) {
        Map<String,Object> map = new HashMap<>();
        questionPoolUseCase.saveQuestionPool(createDto);
        map.put("message","Command completed successfully");
        map.put("data", createDto);
        return map;
    }

    @PutMapping("question-pool/{questionPoolId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateQuestionPoolById(@PathVariable Integer questionPoolId, @Valid @RequestBody QuestionPoolCreateDto updateDto)
    {
        Map<String,Object> map = new HashMap<>();
        questionPoolUseCase.updateQuestionPool(questionPoolId, updateDto);
        map.put("message","Question Pool updated successfully");
        map.put("data",updateDto);
        return map;
    }

    @DeleteMapping("question-pool/{questionPoolId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteQuestionPoolById(@PathVariable Integer questionPoolId) {
        Map<String,Object> map = new HashMap<>();
        questionPoolUseCase.deleteQuestionPoolById(questionPoolId);
        map.put("message","Question Pool deleted successfully");
        return map;
    }

}
