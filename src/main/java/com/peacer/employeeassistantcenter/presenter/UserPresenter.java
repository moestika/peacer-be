package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.UserAllDto;
import com.peacer.employeeassistantcenter.dto.UserCreateDto;
import com.peacer.employeeassistantcenter.dto.UserUpdateDto;
import com.peacer.employeeassistantcenter.service.UserUseCase;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/")
@Api(value = "User Management", description = "Presenter for user data as authentication gateway")
public class UserPresenter
{
    @Autowired
    UserUseCase useCase;

    @GetMapping("users")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getUsers() {
        List<UserAllDto> userAllDtoList = useCase.findAllUsers();
        Map<String, Object> map = new HashMap<>();
        map.put("message","Command completed successfully");
        map.put("total", userAllDtoList.size());
        map.put("data", userAllDtoList);
        return map;
    }

    @GetMapping("user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> getUser(@PathVariable Integer userId) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "Command completed successfully");
        map.put("data", useCase.findUserById(userId));
        return map;
    }

    @PostMapping("user")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createUser(@RequestBody UserCreateDto userCreateDto) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "User created successfully");
        map.put("data", useCase.createUserData(userCreateDto));
        return map;
    }

    @PutMapping("user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> updateUser(@PathVariable Integer userId, @RequestBody UserUpdateDto userUpdateDto) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", "User updated successfully");
        map.put("data",useCase.updateUserData(userUpdateDto,userId));
        return map;
    }

    @DeleteMapping("user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> deleteUser(@PathVariable Integer userId) {
        useCase.deleteUserData(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("message", "User deleted successfully");
        return map;
    }
}
