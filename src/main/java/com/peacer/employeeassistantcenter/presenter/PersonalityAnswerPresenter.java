package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.dto.PersonalityAnswerDto;
import com.peacer.employeeassistantcenter.exceptions.ResourceNotFound;
import com.peacer.employeeassistantcenter.model.PersonalityAnswerModel;
import com.peacer.employeeassistantcenter.service.PersonalityAnswerUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
public class PersonalityAnswerPresenter
{
    @Autowired
    PersonalityAnswerUseCase useCase;

    @PostMapping("personality_answer")
    public Map<String, Object> createAnswer(@RequestBody PersonalityAnswerDto createDto)
    {
        useCase.savePersonalityAnswerData(createDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data inserted");
        feedback.put("data", createDto);
        return feedback;
    }

    @GetMapping("personality_answers")
    public Map<String, Object> getAnswers()
    {
        List<PersonalityAnswerDto> list = useCase.findAllPersonalityAnswer();
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("count", list.size());
        feedback.put("data", list);
        return feedback;
    }

    @GetMapping("personality_answer/{id}")
    public Map<String, Object> getAnswer(@PathVariable Integer id)
    {
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data found");
        feedback.put("data", useCase.findPersonalityAnswerById(id));
        return feedback;
    }

    @PutMapping("personality_answer/{id}")
    public Map<String, Object> updateAnswer(@PathVariable Integer id, @RequestBody PersonalityAnswerDto updateDto)
    {
        useCase.updatePersonalityAnswerData(id, updateDto);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data udpated");
        return feedback;
    }

    @DeleteMapping("personality_answer/{id}")
    public Map<String, Object> deleteAnswer(@PathVariable Integer id)
    {
        useCase.deletePersonalityAnswerData(id);
        Map<String, Object> feedback = new HashMap<>();
        feedback.put("message", "data deleted");
        return feedback;
    }
}
