package com.peacer.employeeassistantcenter.presenter;

import com.peacer.employeeassistantcenter.model.GenderModel;
import com.peacer.employeeassistantcenter.service.GenderUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class GenderPresenter {
    
    @Autowired
    GenderUseCase genderUseCase;

    @GetMapping("genders")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> findAllGenders() {
        Map<String,Object> map = new HashMap<>();
        List<GenderModel> genderModelList = genderUseCase.findAllGenders();
        map.put("message","Command completed successfully");
        map.put("total",genderModelList.size());
        map.put("data",genderModelList);
        return map;
    }

    @PostMapping("gender")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> createGender(@Valid @RequestBody GenderModel genderModel) {
        Map<String,Object> map = new HashMap<>();
        genderUseCase.saveGender(genderModel);
        map.put("message","Gender created successfully");
        map.put("data", genderModel);
        return map;
    }

    @PutMapping("gender/{genderId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updateGender(@PathVariable Integer genderId, @Valid @RequestBody GenderModel genderModel) {
        Map<String,Object> map = new HashMap<>();
        Optional<GenderModel> optionalGenderModel = genderUseCase.findGenderById(genderId);
        optionalGenderModel.get().setGenderName(genderModel.getGenderName());
        genderUseCase.saveGender(optionalGenderModel.get());
        map.put("message","Gender updated successfully");
        map.put("data", optionalGenderModel.get());
        return map;
    }

    @DeleteMapping("gender/{genderId}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> deleteGender(@PathVariable Integer genderId) {
        Map<String,Object> map = new HashMap<>();
        genderUseCase.deleteGenderById(genderId);
        map.put("message","Gender deleted successfully");
        return map;
    }

}
