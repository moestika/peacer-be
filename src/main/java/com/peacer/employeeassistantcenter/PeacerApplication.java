package com.peacer.employeeassistantcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeacerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeacerApplication.class, args);
	}

}
