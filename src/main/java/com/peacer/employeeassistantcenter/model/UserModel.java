package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "users")
public class UserModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    private String userEmail;

    private String userPassword;

    private String userStatus;

    @ManyToOne
    @JoinColumn(name = "roleId")
    private RoleModel role;

    @OneToOne(mappedBy = "user")
    private EmployeeModel employee;

    @OneToOne(mappedBy = "user")
    private CounselorModel counselor;

    @OneToMany(mappedBy = "user")
    private List<FeedbackModel> feedbacks;

    @OneToMany(mappedBy = "user")
    private List<ArticleModel> articles;

    @OneToMany(mappedBy = "creator")
    private List<QuestionPoolModel> topics;

    @OneToMany(mappedBy = "responder")
    private List<QuestionPoolModel> responses;

    @OneToMany(mappedBy = "mover")
    private List<QuestionPoolModel> moves;

    @OneToMany(mappedBy = "creator")
    private List<QuestionTreeModel> tree;

    @OneToMany(mappedBy = "personalityTestAuthor")
    private List<PersonalityTestModel> personalityTests;

    @OneToMany(mappedBy = "user")
    private List<PopupFeelingModel> popupFeeling;

//    @OneToMany(mappedBy = "user")
//    private List<PersonalityUserResultModel> resultUsers;

    @OneToMany(mappedBy = "userPicks")
    private List<PersonalityUserAnswerModel> userPicks;

}
