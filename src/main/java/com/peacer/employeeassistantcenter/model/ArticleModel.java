package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "articles")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ArticleModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer articleId;

    @ManyToOne
    @JoinColumn(name = "creatorUserId")
    private UserModel user;

    private String articleType;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private CategoryModel category;

    private String articleTitle;

    private String articleContent;

    private Date articleDate;

    private String articleStatus;

    @OneToMany(mappedBy = "article")
    private List<ImageModel> images;

}
