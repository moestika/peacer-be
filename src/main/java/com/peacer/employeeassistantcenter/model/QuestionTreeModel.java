package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.apache.catalina.User;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "question_trees")
public class QuestionTreeModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer questionTreeId;

    @ManyToOne
    @JoinColumn(name = "creatorUserId")
    private UserModel creator;

    @ManyToOne
    @JoinColumn(name = "optionParentId", nullable = true)
    private OptionModel option;

    @OneToMany(mappedBy = "questionTree")
    private List<OptionModel> options;

    private String question;

    private Date questionDate;
}
