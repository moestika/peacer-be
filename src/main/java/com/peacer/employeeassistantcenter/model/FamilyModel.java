package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "families")
public class FamilyModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer familyId;

    @ManyToOne
    @JoinColumn(name = "employeeId")
    private EmployeeModel employee;

    private String familyStatus;

    private String familyName;

    private String familyBirthPlace;

    private Date familyBirthDate;

    private String familyAddress;

    private String familyLatestEducation;

    private String familyProfession;

}
