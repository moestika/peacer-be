package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "options")
public class OptionModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer optionId;

    @OneToMany(mappedBy = "option")
    private List<QuestionTreeModel> tree;

    @ManyToOne
    @JoinColumn(name = "questionId")
    private QuestionTreeModel questionTree;

    private String solution;

    private String optionType;
}
