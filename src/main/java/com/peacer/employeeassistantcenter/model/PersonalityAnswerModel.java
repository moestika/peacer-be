package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "personality_answers")
public class PersonalityAnswerModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer answerId;

    private String answer;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private PersonalityTestModel question;

    @OneToMany(mappedBy = "userAnswer")
    private List<PersonalityUserAnswerModel> answers;
}
