package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "personality_tests")
public class PersonalityTestModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer personalityTestId;

    private String personalityTestType;

    private Date personalityTestDateCreated;

    private String personalityTestQuestion;

    @ManyToOne
    @JoinColumn(name = "personalityTestAuthor")
    private UserModel personalityTestAuthor;

    @OneToMany(mappedBy = "question")
    private List<PersonalityAnswerModel> answer;

}
