package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "categories")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CategoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer categoryId;

    private String categoryName;

    private String categoryType;

    @OneToMany(mappedBy = "category")
    private List<QuestionPoolModel> topics;

    @OneToMany(mappedBy = "category")
    private List<ArticleModel> articles;

}
