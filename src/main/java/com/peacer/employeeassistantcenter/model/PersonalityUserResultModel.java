package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "UserPersonalityResult")
public class PersonalityUserResultModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userResultId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserModel user;

    @ManyToOne
    @JoinColumn(name = "resultId")
    private PersonalityResultModel result;

}
