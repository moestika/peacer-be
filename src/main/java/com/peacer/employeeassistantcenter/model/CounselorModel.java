package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "counselors")
public class CounselorModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer counselorId;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    private UserModel user;

    @OneToMany(mappedBy = "counselor")
    private List<RatingModel> ratings;

    private String counselorNik;

    private String counselorName;

    private String counselorCompany;

    private String counselorExperience;

    private String counselorLatestEducation;

    private String counselorContractStart;

    private String counselorContractEnd;

    private String counselorPhoto;

}
