package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "question_pools")
public class QuestionPoolModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer questionPoolId;

    @ManyToOne
    @JoinColumn(name = "creatorUserId")
    private UserModel creator;

    @ManyToOne
    @JoinColumn(name = "responseUserId")
    private UserModel responder;

    @ManyToOne
    @JoinColumn(name = "moverUserId")
    private UserModel mover;

    @ManyToOne
    @JoinColumn(name = "questionCategoryId")
    private CategoryModel category;

    private String questionTicketNumber;

    private String questionTopic;

    private String questionQuestion;

    private String questionResponse;

    private Date questionDate;

    private String questionStatus;

}
