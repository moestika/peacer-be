package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "employees")
public class EmployeeModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer employeeId;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    private UserModel user;

    private String employeeNip;

    private String employeeFullName;

    private String employeeNickName;

    private String employeeCompanyName;

    private Date employeeBirthDate;

    private String employeeJobPosition;

    private String employeeJobLocation;

    private String employeeGender;

    private String employeePhoto;

    private String employeeStatus;

    private String employeeSocialIg;

    private String employeeSocialFb;

    private String employeeSocialTw;

    private String employeeSocialLi;

    @OneToMany(mappedBy = "employee")
    private List<FamilyModel> families;

    @OneToMany(mappedBy = "employee")
    private List<RatingModel> rateGiver;

    @OneToMany(mappedBy = "employeeCounselor")
    private List<RatingModel> employeeAsCounselor;

}
