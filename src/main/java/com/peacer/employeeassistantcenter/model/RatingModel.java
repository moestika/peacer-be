package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "ratings")
public class RatingModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ratingId;

    @ManyToOne
    @JoinColumn(name = "counselorId")
    private CounselorModel counselor;

    @ManyToOne
    @JoinColumn(name = "employeeId")
    private EmployeeModel employee;

    @ManyToOne
    @JoinColumn(name = "employeeCounselorId",nullable = true)
    private EmployeeModel employeeCounselor;

    private Integer ratingScore;

}
