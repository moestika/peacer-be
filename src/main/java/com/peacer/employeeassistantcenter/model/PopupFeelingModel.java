package com.peacer.employeeassistantcenter.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Table(name = "popup_feelings")
public class PopupFeelingModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer popupId;

    private Date popupDate;

    private String popupChosen;

    @ManyToOne
    @JoinColumn(name = "popup_user_id")
    private UserModel user;
}
